import unittest
from src.messages.table_message import TableMessage
from blessed import Terminal

class TestTableMessage(unittest.TestCase):
    

    def test_basic_functionality(self):
        term = Terminal()
        data = [["Name", "Age"], ["Alice", 30], ["Bob", 25]]
        table_msg = TableMessage(term, data, max_table_width=80)
        formatted_table = table_msg.format_message()
        self.assertTrue(len(formatted_table) > 0)
    
    def test_min_table_width_exceeds_max(self):
        term = Terminal()
        data = [["Name", "Age"], ["Alice", 30], ["Bob", 25]]
        min_column_width = 20  # Assuming each column has a minimum width of 20 characters
        max_table_width = 30   # Setting a max table width less than the total min width

        with self.assertRaises(ValueError):  # Expecting a ValueError or a custom exception
            table_msg = TableMessage(term, data, max_table_width=max_table_width, min_column_width=min_column_width)
            table_msg.format_message()
    
    def test_basic_table_width(self):
        term = Terminal()
        data = [["Name", "Age"], ["Alice", 30], ["Bob", 25]]
        table_msg = TableMessage(term, data, max_table_width=80)
        formatted_table = table_msg.format_message()
        for row in formatted_table:
            self.assertEqual(len(row),80)

    def test_basic_table_min_column_width(self):
        term = Terminal()
        data = [["Name", "Age"], ["Alice", 30], ["Bob", 25]]
        min_column_width = 20
        table_msg = TableMessage(term, data, max_table_width=80,min_column_width=min_column_width)
        formatted_table = table_msg.format_message()
        for width in table_msg.column_widths:
            self.assertGreaterEqual(width, min_column_width)
    
    def test_basic_table_max_column_width(self):
        term = Terminal()
        data = [["Name", "Age"], ["Alice", 300000000000], ["Bob", 250000000000]]
        max_col_width = [5,12]
        table_msg = TableMessage(term, data, max_table_width=80,max_widths=max_col_width)
        formatted_table = table_msg.format_message()
        widths = table_msg.column_widths
        self.assertEqual(widths[0], max_col_width[0])
        self.assertEqual(widths[1], max_col_width[1])