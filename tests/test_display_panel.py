import unittest
from unittest.mock import Mock, patch
from src.panels.panel import DisplayPanel
from src.utils.color_formatter import ColorFormatter

class TestDisplayPanel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Set up common mock objects and settings
        cls.term = Mock()
        cls.term.width = 100
        cls.term.height = 40
        
        # Mock the location method as a context manager
        cls.mock_context_manager = Mock()
        cls.mock_context_manager.__enter__ = Mock()
        cls.mock_context_manager.__exit__ = Mock(return_value=False)
        cls.term.location = Mock(return_value=cls.mock_context_manager)

        # Mock the apply_color method to return a formatted string
        ColorFormatter.apply_color = Mock(return_value="Formatted string")

    def test_offset_to_right_non_zero_value(self):
        offset_value = 10

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel
            panel = DisplayPanel('results', self.term, offset_from_bottom=7, offset_to_right=offset_value,
                                    bg_color='blue', fg_color='white', 
                                    outline_bottom=True, outline_highlight_bg='yellow', 
                                    term_bg_color='black')
            
        expected_width = self.term.width - offset_value
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_to_right.")
        expected_x = 0
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_to_right.")

    def test_offset_to_right_zero_value(self):
        offset_value = 0

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel
            panel = DisplayPanel('results', self.term, offset_from_bottom=7, offset_to_right=offset_value,
                                    bg_color='blue', fg_color='white', 
                                    outline_bottom=True, outline_highlight_bg='yellow', 
                                    term_bg_color='black')
            
        expected_width = self.term.width - offset_value
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_to_right.")
        expected_x = 0
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_to_right.")

    def test_offset_from_right_non_zero_value(self):
        offset_value = 10

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel
            panel = DisplayPanel('results', self.term, offset_from_bottom=7, offset_from_right=offset_value, 
                                    offset_to_right=0, bg_color='blue', fg_color='white', 
                                    outline_bottom=True, outline_highlight_bg='yellow', 
                                    term_bg_color='black')
            
        expected_width = offset_value
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_from_right.")
        expected_x = self.term.width - expected_width
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_from_right.")

    def test_offset_from_right_zero_value(self):
        offset_value = 0

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel
            panel = DisplayPanel('results', self.term, offset_from_bottom=7, offset_from_right=offset_value, 
                                    offset_to_right=0, bg_color='blue', fg_color='white', 
                                    outline_bottom=True, outline_highlight_bg='yellow', 
                                    term_bg_color='black')
            
        expected_width = self.term.width - offset_value
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_from_right.")
        expected_x = self.term.width - expected_width
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_from_right.")


    def test_invalid_right_offsets(self):
        # Set invalid offsets, where offset_to_right is larger than offset_from_right
        offset_to_right = 30
        offset_from_right = 20

        with self.assertRaises(ValueError):
            # Initialize DisplayPanel with invalid offsets
            with patch('builtins.print') as mock_print:
                DisplayPanel('results', self.term, offset_from_bottom=7, offset_to_right=offset_to_right,
                                offset_from_right=offset_from_right, bg_color='blue', fg_color='white', 
                                outline_bottom=True, outline_highlight_bg='yellow', 
                                term_bg_color='black')

    def test_both_offset_from_left_and_offset_from_right(self):

            with self.assertRaises(ValueError):
                DisplayPanel('invalid_panel', self.term, offset_from_left=10, offset_from_right=20,
                            bg_color='blue', fg_color='white')

    def test_offset_from_left_non_zero_value(self):
        # Initialize a panel with offset_from_left set to 20
        with patch('builtins.print') as mock_print:
            panel = DisplayPanel('offset_test', self.term, offset_from_left=20,
                                 bg_color='blue', fg_color='white')
        
        # Calculate the expected width as the term width minus the offset
        expected_width = self.term.width - 20
        
        # Calculate the expected x starting position as 20
        expected_x = 20
        
        # Assert that panel width and x starting position are set correctly
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_from_left.")
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_from_left.")

    def test_offset_from_left_zero_value(self):
        # Initialize a panel with offset_from_left set to 0
        with patch('builtins.print') as mock_print:
            panel = DisplayPanel('offset_test', self.term, offset_from_left=0,
                                bg_color='blue', fg_color='white')
        
        # Calculate the expected width as the term width
        expected_width = self.term.width
        
        # Calculate the expected x starting position as 0
        expected_x = 0
        
        # Assert that panel width and x starting position are set correctly
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_from_left=0.")
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_from_left=0.")

    def test_offset_from_left_static_width(self):
        # Initialize a panel with offset_from_left and a static width of 80
        with patch('builtins.print') as mock_print:
            panel = DisplayPanel('offset_test', self.term, offset_from_left=20, width=80,
                                bg_color='blue', fg_color='white')
        
        # Calculate the expected width as the static width
        expected_width = 80
        
        # Calculate the expected x starting position as 20
        expected_x = 20
        
        # Assert that panel width and x starting position are set correctly
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_from_left and static width.")
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_from_left and static width.")

    def test_offset_from_left_and_offset_to_right(self):
        # Initialize a panel with offset_from_left of 20 and offset_to_right of 10
        with patch('builtins.print') as mock_print:
            panel = DisplayPanel('offset_test', self.term, offset_from_left=20, offset_to_right=10,
                                bg_color='blue', fg_color='white')
            
        # Calculate the expected width as term.width - offset_from_left - offset_to_right
        expected_width = self.term.width - 20 - 10
        
        # Calculate the expected x starting position as 20
        expected_x = 20
        
        # Assert that panel width and x starting position are set correctly
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly with offset_from_left and offset_to_right.")
        self.assertEqual(panel.panel_x, expected_x, "X starting position not set correctly with offset_from_left and offset_to_right.")

    def test_static_height_non_zero(self):
        # Initialize a panel with offset_from_left and a static width of 80
        height = 20
        with patch('builtins.print') as mock_print:
            panel = DisplayPanel('offset_test', self.term, width=80, height=height,
                                bg_color='blue', fg_color='white')
        
        # Calculate the expected width as the static width
        expected_height = 20
        
        # Calculate the expected x starting position as 20
        expected_y = 0
        
        # Assert that panel width and x starting position are set correctly
        self.assertEqual(panel.panel_height, expected_height, "Panel width not set correctly with offset_from_left and static width.")
        self.assertEqual(panel.panel_y, expected_y, "X starting position not set correctly with offset_from_left and static width.")  
    
    def test_static_height_with_offset_from_top(self):
        # Define the offset from the top
        offset_from_top = 10
        static_height = 20

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with an offset from the top
            panel = DisplayPanel('offset_test', self.term, height=static_height,
                                offset_from_top=offset_from_top,
                                bg_color='blue', fg_color='white')
        
        # Calculate the expected height as the static height
        expected_height = static_height
        
        # Calculate the expected y starting position as the offset from the top
        expected_y = offset_from_top
        
        # Assert that panel height is set correctly
        self.assertEqual(panel.panel_height, expected_height, 
                        "Panel height not set correctly with static height.")
        
        # Assert that Y starting position is set correctly
        self.assertEqual(panel.panel_y, expected_y, 
                        "Y starting position not set correctly with offset_from_top.")

    def test_dynamic_height_with_offset_from_bottom_zero(self):
        # Define the offset from the bottom
        offset_from_bottom = 0

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with offset_from_bottom and no offset_from_top
            panel = DisplayPanel('dynamic_height_test', self.term, 
                                offset_from_top=0,offset_from_bottom=offset_from_bottom,
                                bg_color='blue', fg_color='white')

        # Calculate the expected height as the terminal height minus offset_from_bottom
        expected_height = self.term.height - offset_from_bottom

        # The Y starting position should be 0 as there's no offset_from_top
        expected_y = 0

        # Assert that panel height is dynamically set correctly
        self.assertEqual(panel.panel_height, expected_height,
                        "Panel height not set correctly with dynamic height calculation.")

        # Assert that Y starting position is set correctly
        self.assertEqual(panel.panel_y, expected_y,
                        "Y starting position not set correctly without offset_from_top.")

    def test_dynamic_height_with_non_zero_offset_from_bottom(self):
        # Define a non-zero offset from the bottom
        offset_from_bottom = 5

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with offset_from_bottom and no offset_from_top
            panel = DisplayPanel('dynamic_height_test', self.term,
                                offset_from_top=0,offset_from_bottom=offset_from_bottom,
                                bg_color='blue', fg_color='white')

        # Calculate the expected height as the terminal height minus offset_from_bottom
        expected_height = self.term.height - offset_from_bottom

        # The Y starting position should still be 0 as there's no offset_from_top
        expected_y = 0

        # Assert that panel height is dynamically set correctly
        self.assertEqual(panel.panel_height, expected_height,
                        "Panel height not set correctly with dynamic height calculation and non-zero offset_from_bottom.")

        # Assert that Y starting position is set correctly
        self.assertEqual(panel.panel_y, expected_y,
                        "Y starting position not set correctly without offset_from_top.")

    def test_dynamic_height_with_both_offsets(self):
        # Define both offsets
        offset_from_top = 10
        offset_from_bottom = 5

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with both offsets
            panel = DisplayPanel('dynamic_height_test', self.term,
                                offset_from_top=offset_from_top,
                                offset_from_bottom=offset_from_bottom,
                                bg_color='blue', fg_color='white')

        # Calculate the expected height as the terminal height minus both offsets
        expected_height = self.term.height - offset_from_top - offset_from_bottom

        # The expected y starting position should be the offset_from_top
        expected_y = offset_from_top

        # Assert that panel height is dynamically set correctly
        self.assertEqual(panel.panel_height, expected_height,
                        "Panel height not set correctly with dynamic height calculation and both offsets.")

        # Assert that Y starting position is set correctly
        self.assertEqual(panel.panel_y, expected_y,
                        "Y starting position not set correctly with offset_from_top.")
    
    def test_resize_dimension_config_static_width(self):
        # Define static width for dimension_config
        static_width = 20
        dimension_config = {
            'static': {
                'panel_width': static_width
            }
        }

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with a dimension_config
            panel = DisplayPanel('results', self.term, dimension_config=dimension_config,
                                bg_color='blue', width=100,height=100, fg_color='white',
                                outline_bottom=True, outline_highlight_bg='yellow',
                                term_bg_color='black')

            # Call the resize_dimension_config_static_width method
            panel.resize_panel(setting_name='static')

            # Assert that the panel width is set to the static width defined in dimension_config
            self.assertEqual(panel.panel_width, static_width, "Panel width not set correctly to static width in dimension_config.")
        
    def test_resize_horizontal_dynamic_panel(self):
        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel
            panel = DisplayPanel('results', self.term, offset_from_bottom=7,
                                    bg_color='blue', fg_color='white', 
                                    outline_bottom=True, outline_highlight_bg='yellow', 
                                    term_bg_color='black')

            # Resize the panel
            panel.resize_panel(term_width=self.term.width - 20)

            expected_width = self.term.width - 20
            self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly after resize.")

    def test_resize_horizontal_static_panel(self):
        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel
            panel = DisplayPanel('results', self.term, offset_from_bottom=7, width=100, 
                                    offset_to_right=0, bg_color='blue', fg_color='white', 
                                    outline_bottom=True, outline_highlight_bg='yellow', 
                                    term_bg_color='black')
            
            panel.resize_panel(panel_width=self.term.width - 20)

        expected_width = self.term.width - 20
        self.assertEqual(panel.panel_width, expected_width, "Panel width not set correctly after resize.")

    def test_resize_vertical_dynamic_panel_with_offset(self):
        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with initial height and offset_from_bottom
            self.term.width = 110
            offset_from_bottom = 10
            panel = DisplayPanel('results', self.term,
                                offset_from_bottom=offset_from_bottom,
                                bg_color='blue', fg_color='white',
                                outline_bottom=True, outline_highlight_bg='yellow',
                                term_bg_color='black')

            # Set new terminal height
            new_term_height = 100
            panel.resize_panel(term_height=new_term_height)

            # Calculate the expected panel height after resizing
            expected_height = new_term_height - offset_from_bottom
            self.assertEqual(panel.panel_height, expected_height,
                            "Panel height not set correctly after vertical resize with offset_from_bottom.")


    def test_resize_vertical_static_panel(self):
        static_height = 50  # Define a static height for the panel

        with patch('builtins.print') as mock_print:
            # Initialize DisplayPanel with static height
            panel = DisplayPanel('results', self.term, height=static_height,
                                bg_color='blue', fg_color='white', 
                                outline_bottom=True, outline_highlight_bg='yellow', 
                                term_bg_color='black')

            # Resize the terminal height
            new_panel_height = 100
            panel.resize_panel(panel_height=new_panel_height)

            # The panel height should remain as the static height, unaffected by terminal height resize
            self.assertEqual(panel.panel_height, new_panel_height, "Panel height changed incorrectly after resize.")

# Run the test
if __name__ == '__main__':
    unittest.main()
