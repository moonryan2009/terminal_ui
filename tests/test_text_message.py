import unittest
from blessed import Terminal
from src.messages.text_message import TextMessage

class TestTextMessage(unittest.TestCase):
    def setUp(self):
        self.term = Terminal()
        self.sample_text = "Test Message"

    def test_text_message_creation(self):
        # Test creating a TextMessage instance
        text_msg = TextMessage(self.term, self.sample_text)
        self.assertIsNotNone(text_msg)
        self.assertEqual(text_msg.text, self.sample_text)
    
    def test_format_message(self):
        # Test formatting the text message
        text_msg = TextMessage(self.term, self.sample_text)
        formatted_message = text_msg.format_message()
        self.assertIsNotNone(formatted_message)
        self.assertTrue(isinstance(formatted_message, list))
        self.assertEqual(len(formatted_message), 1)
    
    def test_center_alignment(self):
        # Test center alignment
        text_msg = TextMessage(self.term, self.sample_text, max_width=20, alignment='center')
        formatted_message = text_msg.format_message()
        self.assertIsNotNone(formatted_message)
        self.assertTrue(isinstance(formatted_message, list))
        self.assertEqual(len(formatted_message), 1)
    
        # Get the formatted line
        line = formatted_message[0]

        # Find the text position within the line
        text_start = line.find(self.sample_text)
        text_end = text_start + len(self.sample_text)

        # Check the number of spaces before and after the text
        spaces_before = text_start
        spaces_after = len(line) - text_end

        # Assert that there are equal spaces in the front and back of the text
        self.assertEqual(spaces_before, spaces_after)

    def test_right_alignment(self):
        # Test right alignment
        max_width = 20
        text_msg = TextMessage(self.term, self.sample_text, max_width=max_width, alignment='right')
        formatted_message = text_msg.format_message()
        self.assertIsNotNone(formatted_message)
        self.assertTrue(isinstance(formatted_message, list))
        self.assertEqual(len(formatted_message), 1)
        
        # Get the formatted line
        line = formatted_message[0]

        # Find the text position within the line
        text_start = line.find(self.sample_text)
        text_end = text_start + len(self.sample_text)

        # Check the number of spaces after the text
        spaces_after = len(line) - text_end

        # Assert that there are no spaces before the text
        self.assertEqual(spaces_after, 0)

        # Assert that there are additional spaces after the text
        self.assertGreater(text_start, 0)

    def test_long_string_wrapping(self):
        # Test a long string that exceeds max width
        max_width = 20  # Set a small max width
        long_text = "This is a long string that exceeds the max width."
        text_msg = TextMessage(self.term, long_text, max_width=max_width)
        formatted_message = text_msg.format_message()
        self.assertIsNotNone(formatted_message)
        self.assertTrue(isinstance(formatted_message, list))
        self.assertEqual(len(formatted_message), 3)  # Expect 2 lines

        # Check if the lines exceed the max width
        for line in formatted_message:
            self.assertLessEqual(len(line), max_width)

    def test_adjust_max_width(self):
        # Test creating a TextMessage with max_width of 20
        initial_max_width = 20
        text_msg = TextMessage(self.term, self.sample_text, max_width=initial_max_width, alignment='left')
        
        # Check that the initial max_width is set correctly
        self.assertEqual(text_msg.max_width, initial_max_width)
        
        # Adjust the max_width to 50
        new_max_width = 50
        text_msg.max_width = new_max_width
        
        # Check that the max_width has been adjusted
        self.assertEqual(text_msg.max_width, new_max_width)
        
        # Format the message and check the length of the formatted message
        formatted_message = text_msg.format_message()
        self.assertIsNotNone(formatted_message)
        self.assertTrue(isinstance(formatted_message, list))
        self.assertEqual(len(formatted_message), 1)  # Expect 1 line

    def test_reduce_max_width(self):
        # Test creating a TextMessage with max_width of 50
        initial_max_width = 50
        text_msg = TextMessage(self.term, self.sample_text, max_width=initial_max_width)

        # Check that the initial max_width is set correctly
        self.assertEqual(text_msg.max_width, initial_max_width)

        # Adjust the max_width to 20
        new_max_width = 20
        text_msg.max_width = new_max_width

        # Check that the max_width has been adjusted
        self.assertEqual(text_msg.max_width, new_max_width)

        # Format the message and check the length of the formatted message
        formatted_message = text_msg.format_message()
        self.assertIsNotNone(formatted_message)
        self.assertTrue(isinstance(formatted_message, list))
        self.assertEqual(len(formatted_message), 3)  # Expect 3 lines