import os
from src.panels.panel import DisplayPanel


if __name__ == "__main__":
    from blessed import Terminal

    # Initialize a terminal
    term = Terminal()

    os.system('cls')

    resize_configs = {
        'small': {'panel_width': 40, 'panel_height': 20},
        'medium': {'panel_width': 80, 'panel_height': 30},
        'large': {'panel_width': 120, 'panel_height': 40}
    }

    # Create a sample panel with an outline
    my_panel = DisplayPanel("Sample Panel", term, panel_x=2, panel_y=2, width=80, height=10,
                            fg_color="white", bg_color="blue",
                            outline_top=True, outline_bottom=True,
                            outline_left=True, outline_right=True,
                            outline_horizontal_char="-", outline_vertical_char="|",
                            outline_color="green", dimension_config=resize_configs)

    # Print the panel's horizontal and vertical borders
    
    #my_panel.print_horizontal_borders()
    #my_panel.print_vertical_borders()

    # Add some sample data
    sample_data = [
        "Lorem ipsum dolor sit ametaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,",
        "consectetur adipiscing elit.",
        "Sed do eiusmod tempor incididunt",
        "ut labore et dolore magna aliqua.",
        "Lorem ipsum dolor sit amet,",
        "consectetur adipiscing elit.",
        "Sed do eiusmod tempor incididunt",
        "ut labore et dolore magna aliqua.",
        "Lorem ipsum dolor sit amet,",
        "consectetur adipiscing elit.",
        "Sed do eiusmod tempor incididunt",
        "ut labore et dolore magna aliqua."
    ]
    #my_panel.add_data(sample_data, fg_color="yellow")
    table_data = [
        ["ID", "Header2", "Header3", "Header4"]
    ]

    # Loop to generate 50 rows
    for i in range(1, 51):  # 1 to 50 inclusive
        row = [str(i), f"Row{i}Col2", f"Row{i}Col3", f"Row{i}Col4"]
        table_data.append(row)

    my_panel.add_table_message(table_data, max_table_width=78, max_widths=[2,None,None,None])


    # Format and display the data
    #formatted_data = my_panel.format_data(sample_data)
    my_panel.display_data()

    with term.cbreak():
        while True:
            key = term.inkey(timeout=0.1)
            if key == 'q':
                break  # Exit the loop if 'q' is pressed
        
            # Add key bindings for resizing
            if key == 's':  # Small size
                my_panel.resize_panel('small')
            elif key == 'm':  # Medium size
                my_panel.resize_panel('medium')
            elif key == 'l':  # Large size
                my_panel.resize_panel('large')

            if key.code == term.KEY_DOWN:
                my_panel.scroll_down()  # Scroll down by 'scroll_step' lines
            elif key.code == term.KEY_UP:
                my_panel.scroll_up()  # Scroll up by 'scroll_step' lines

    # You can add more data and display it as needed

    # Clear the panel (placeholder method)
    #my_panel.clear()