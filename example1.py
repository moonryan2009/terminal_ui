# import the UI class
from src.ui.terminal_ui import TerminalUI 
from src.panels.panel import DisplayPanel # Replace with the actual module name where UI is defined
from src.messages.text_message import TextMessage
from blessed import Terminal
import os
import platform

    # Initialize a terminal
term = Terminal()

# Create an instance of UI with specific minimum size requirements
#ui = TerminalUI(fg_color='white',bg_color='black', min_size_x=80, min_size_y=25)

# Call the check_min_size method to test it
if platform.system() == "Windows":
    os.system('cls')
else:
        # Covers macOS and Unix-like systems
    os.system('clear')
panel = DisplayPanel('box', term, width=80, offset_from_bottom=5, offset_to_bottom=0, bg_color='red', term_bg_color='black', dimension_config={'new': {'panel_width': 20}})
panel.clear_panel()
panel.resize_panel(term_height=term.height-5)
print()
#ui.check_min_size()
#ui.panels['results'].init_panel()
#ui.add_panel(name='messages', offset_to_bottom=7, offset_from_bottom=4, offset_to_right=0, bg_color='blue', fg_color='white', term_bg_color='black')
#ui.add_panel(name='prompt', offset_to_bottom=3, offset_from_bottom=0, width=80, bg_color='green', fg_color='white')
#table_data = [["Column1", "Column2", "Column3"]]  # Header row
#for i in range(1, 51):
#    table_data.append([f"Row{i}Col1", f"Row{i}Col2", f"Row{i}Col3"])
#ui.panels['results'].add_table_message(table_data, max_table_width=ui.panels['results'].panel_width - 4)
#ui.add_hotkey('F1',ui.toggle_help_panel)
#ui.add_hotkey('F12',ui.toggle_debug_panel)
#ui.run()
#ui.check_min_size()
#ui.add_panel(name='debug', offset_from_top=0, offset_from_left=80, bg_color='white', fg_color='black')
#ui.panels['results'].init_panel()
#ui.panels['messages'].add_data('This is a long string that exceeds the max width. This is a long string that exceeds the max width.')
#ui.panels['prompt'].add_data(f"{len(ui.panels['messages'].formatted_lines)}")
#ui.panels['messages'].display_data()
#ui.panels['prompt'].display_data()

#ui.panels['results'].print_borders(highlight_selection=True, highlight_selection_bg="yellow")
#ui.panels['results'].print_borders(highlight_selection=False)

#long_text = "This is a long string that exceeds the max width."
#text_msg = TextMessage(term, long_text, max_width=20)
#print(text_msg.text)
#formatted_message = text_msg.format_message()
#print(formatted_message)