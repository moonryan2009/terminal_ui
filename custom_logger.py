import logging
from ui import UI

import logging

def setup_logging(filename='app.log', level=logging.INFO):
    # Configure the logging level and format
    logging.basicConfig(level=level, 
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        filename=filename,  # Log to a file
                        filemode='a')  # Append mode

    # File handler for logging to a file
    file_handler = logging.FileHandler(filename)
    file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(file_formatter)
    logging.getLogger('').addHandler(file_handler)

    


