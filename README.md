# TerminalUI Framework


## Overview
The TerminalUI Framework is an advanced, modular, terminal-based user interface framework designed to enhance developer productivity and streamline the management of complex applications from the command line. It incorporates a dynamic menu system, application launching capabilities, and extensive hotkey management, making it an essential tool for developers, system administrators, and power users.

## Features
* Dynamic Menu System: Navigate through intuitive menus for efficient access to applications and configurations.
* Application Launcher: Easily launch and manage terminal-based applications with an integrated launcher system.
* Hotkey Management: Utilize customizable hotkeys for quick actions and navigation, improving workflow efficiency.
* Modular Design: Designed for extensibility and customization, allowing for easy adaptation to specific project requirements.
* Comprehensive Logging: Built-in logging capabilities for easy troubleshooting and system monitoring.

## Extending the Framework with Custom Applications

The TerminalUI Framework is designed for extensibility, enabling developers to add custom applications by implementing the `BaseApp` class. This class offers a foundational structure for application development, including built-in methods for input and output operations, as well as menu integration.

### Implementing the `BaseApp` Class

To add applications to the framework, developers should create a subclass of `BaseApp` and implement its methods. The `BaseApp` class provides the following functionalities:

- **Input and Output Callbacks**: Methods are provided for capturing user input and displaying data, which facilitate interaction with the terminal UI.
- **Data Store Integration**: Applications have access to a local `DataStore` for state management and can integrate with a menu-specific data store for sharing data across applications and menus.
- **Menu Integration**: Applications can define their own menu logic for seamless integration with the framework's dynamic menu system.

#### Key Components of `BaseApp`:

- `input_callback`: A function for capturing user input.
- `display_callback`: A function for displaying data in a specified panel.
- `display_table_callback`: A function for displaying tabular data.
- `menu_callback`: An optional function for custom menu logic.
- `local_store`: An instance of `DataStore` for application-specific data management.
- `menu_store`: An optional data store for integrating with the menu system.

### Example Application Implementation

```python
from src.base_app import BaseApp

class MyCustomApp(BaseApp):
    def run(self):
        # Insert application-specific logic here
        user_input = self.capture_user_input()
        self.display_data(f"User input: {user_input}", panel="main")

    # Implement additional methods as needed
```

## Setting Up and Using the TerminalUI

This section provides a step-by-step guide to setting up the TerminalUI framework in your project and demonstrates how to initialize the user interface, configure panels, and integrate applications.

### Initial Setup

First, ensure you have imported the necessary modules and configured logging according to your project's requirements. Here's an example setup:

```python
from src.ui.terminal_ui import TerminalUI  # Adjust the import path as needed
from blessed import Terminal
import logging

# Optional: Setup logging as per your project's requirement
# setup_logging()
```


## Initializing the TerminalUI

Create an instance of TerminalUI with specific configurations such as foreground color, background color, minimum size requirements, and logging configurations:

```python
term = Terminal()
ui = TerminalUI(fg_color='white', bg_color='black',
                min_size_x=80, min_size_y=25,
                logging_file='app.log', logging_level=logging.DEBUG,
                highlight_selection_bg='white')
```

## Configuring Panels

Add panels to the UI for different purposes, such as displaying results, messages, and prompts. Here's how to add panels:

```python
ui.add_panel(name='results', offset_to_bottom=7, fg_color='white', outline_bottom=True)
ui.add_panel(name='messages', offset_from_bottom=7, offset_to_bottom=3, fg_color='white', term_bg_color='black')
ui.add_panel(name='prompt', offset_from_bottom=3, offset_to_bottom=0, fg_color='white', term_bg_color='black')
```

Set the main display, message, and input panels:

```python
ui.set_main_display_panel('results')
ui.set_message_panel('messages')
ui.set_input_panel('prompt')


## Integrating Hotkeys and Menus

Configure hotkeys for easy access to functionalities like help or debug panels:

```python
ui.add_hotkey('F1', toggle_panel='help_ui', label='Help') 
ui.add_hotkey('F12', toggle_panel='debug_ui', label='Debug')
```

Create the main menu and add items to it, linking to settings or applications:

```python
ui.create_menu('main', is_main_menu=True)
ui.add_menu_item('main', 'settings', 'Settings', 'test')  # Replace 'test' with the actual navigation target
ui.add_menu_item('main', 'app1', 'App1', 'app1')  # Ensure 'app1' is registered in your application store
```

## Running the TerminalUI
Finally, to bring your TerminalUI to life, call the run method:

```python
ui.run()
```

## Integrating Applications

To integrate applications, ensure they inherit from the BaseApp class and implement necessary methods. Register your application with the UI to make it accessible:

```python
from apps.app.app import MyApp  # Adjust the import path as needed

ui.create_and_store_app('app1', MyApp)
```

This setup provides a foundational structure for your TerminalUI application. Customize the configurations, panels, hotkeys, and menus according to your project's needs.

## To-Do

- **Save Color Schemas in a JSON File**: Implement functionality to save and manage color schemes for the terminal UI within a JSON file. This will allow users to switch between predefined color schemes easily and possibly share their configurations with others.

- **Terminal UI Settings Menu**: Develop a settings menu within the TerminalUI framework that allows users to customize and save their preferences, including the current color scheme. This menu will enable users to tailor the UI experience to their liking and ensure these settings persist across sessions.

## License
This project is licensed under the MIT License. You are free to use, modify, and distribute it as per the terms of the license.

