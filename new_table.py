import unittest
from src.messages.table_message import TableMessage
from blessed import Terminal

term = Terminal()

        
data = [["Name", "Age"], ["Alice", 30], ["Bob", 25]]
table_msg = TableMessage(term, data, max_table_width=30, min_column_width=20)
formatted_table = table_msg.format_message()

for line in formatted_table:
    print(line)