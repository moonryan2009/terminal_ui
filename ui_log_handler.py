import logging

class UILogHandler(logging.Handler):
    def __init__(self, ui):
        super().__init__()
        self.ui = ui

    def emit(self, record):
        message = self.format(record)
        self.ui.add_debug_message(message)  # Method to add message to the UI