# import the UI class
from src.ui.terminal_ui import TerminalUI  # Replace with the actual module name where UI is defined
from src.messages.text_message import TextMessage
from src.utils.logging_setup import setup_logging
from blessed import Terminal
from apps.app.app import MyApp
import logging

#setup_logging()

    # Initialize a terminal
term = Terminal()

# Create an instance of UI with specific minimum size requirements
ui = TerminalUI(fg_color='white',bg_color='black',
                min_size_x=80, min_size_y=25,
                logging_file = 'app.log', logging_level=logging.DEBUG,
                highlight_selection_bg='white')
# Call the check_min_size method to test it
ui.add_panel(name='results', offset_to_bottom=7, offset_to_right=0,
              fg_color='white', outline_bottom=True)
#ui.check_min_size()
#ui.panels['results'].init_panel()
ui.add_panel(name='messages', offset_from_bottom=7, offset_to_bottom=3,
              offset_to_right=0, fg_color='white',
               term_bg_color='black')
ui.add_panel(name='prompt', offset_from_bottom=3, offset_to_bottom=0,
              offset_to_right=0, fg_color='white',
               term_bg_color='black')
ui.set_main_display_panel('results')
ui.set_message_panel('messages')
ui.set_input_panel('prompt')
#table_data = [["Column1", "Column2", "Column3"]]  # Header row
#for i in range(1, 51):
#    table_data.append([f"Row{i}Col1", f"Row{i}Col2", f"Row{i}Col3"])
#ui.panels['results'].add_table_message(table_data, max_table_width=ui.panels['results'].panel_width - 4)
ui.add_hotkey('F1', toggle_panel='help_ui', excluded_panels=['_ui'], label='Help') 
ui.add_hotkey('F12', toggle_panel='debug_ui', excluded_panels=['_ui'], label='Debug') 
ui.create_menu('main', is_main_menu=True, add_common_items=False)
ui.create_and_store_app('app1', MyApp)
ui.add_menu_item('main', 'settings', 'Settings', 'test')
ui.add_menu_item('main', 'app1', 'App1', 'app1')
ui.run()
#ui.check_min_size()
#ui.add_panel(name='debug', offset_from_top=0, offset_from_left=80, bg_color='white', fg_color='black')
#ui.panels['results'].init_panel()
#ui.panels['messages'].add_data('This is a long string that exceeds the max width. This is a long string that exceeds the max width.')
#ui.panels['prompt'].add_data(f"{len(ui.panels['messages'].formatted_lines)}")
#ui.panels['messages'].display_data()
#ui.panels['prompt'].display_data()

#ui.panels['results'].print_borders(highlight_selection=True, highlight_selection_bg="yellow")
#ui.panels['results'].print_borders(highlight_selection=False)

#long_text = "This is a long string that exceeds the max width."
#text_msg = TextMessage(term, long_text, max_width=20)
#print(text_msg.text)
#formatted_message = text_msg.format_message()
#print(formatted_message)