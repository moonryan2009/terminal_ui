from blessed import Terminal
from typing import List, Optional, Union
import os
import pyperclip


class UI():
    """
    This class represents a user interface for interactive content.

    :attribute term: An instance of the blessed Terminal.
    :type term: Terminal
    :attribute interactive_content_height: Height of the interactive content, defaults to 6
    :type interactive_content_height: int, optional
    :attribute data_height: Height of the data content, calculated based on the terminal height
    :type data_height: int
    :attribute data_content: List to store data
    :type data_content: list
    :attribute prompt_message: Message to display as a prompt
    :type prompt_message: str
    :attribute message: Message to display
    :type message: str
    :attribute data_top: Top position of data
    :type data_top: int
    :attribute user_input: User input
    :type user_input: str
    :attribute data_updated: Indicates if data has been updated
    :type data_updated: bool
    :attribute prompt_updated: Indicates if prompt has been updated
    :type prompt_updated: bool
    :attribute message_updated: Indicates if message has been updated
    :type message_updated: bool
    :attribute input_updated: Indicates if user input has been updated
    :type input_updated: bool
    :attribute debug_section: Indicates if debug mode is enabled, defaults to False
    :type debug_section: bool, optional
    :attribute debug_messages: List to store debug messages
    :type debug_messages: list
    :attribute debug_update: Indicates if debug messages have been updated
    :type debug_update: bool
    :attribute max_debug_lines: Maximum number of debug lines, defaults to 10
    :type max_debug_lines: int, optional
    :attribute color_schemas: Dictionary containing color schemes
    :type color_schemas: dict

    :param debug_section: Flag to show the debug panel
    :type debug_section: bool
    """
    
    def __init__(self, debug_section:bool=False, display_mode:str='dynamic') -> None:
    
        #start postions are in formatin (row,column)
        self.term = Terminal()
        self.display_mode = display_mode 
        self.prev_terminal_width = self.term.width
        #Interactive Section

        #prompt
        self.user_input = ''
        self.is_prompt_updated = False
        self.prompt_message = ''
        self.prompt_start = (self.term.height - 1, 0)
        self.is_input_updated = False
        self.interactive_content_height = 6

        #message
        self.message = ''
        self.message_start = (self.term.height - 2,0)
        self.is_message_updated = False

        #debug
        self.interactive_debug_start = (self.term.height - 4,0)


        #Data Section
        self.data_content = []
        self.is_data_updated = False
        self.data_start = (0, 0)
        self.data_height = self.term.height - self.interactive_content_height
        self.data_top = 0
        self.original_data_content = []
        self.original_data_table = ([],[],None,None)
        self.data_max_width = 80

        #Debug Section
        self.debug_section = debug_section
        self.debug_messages = []
        self.debug_update = False
        self.max_debug_lines = 10
        self.debug_start = (0,85)
        self.debug_min_width = 120

        self.color_schemas = {
            "subdued": {
                "background": self.term.on_black,
                "prompt": self.term.bright_yellow,
                "table_output": self.term.green,
                "table_elements": self.term.bright_blue,
                "text": self.term.white,
                "error": self.term.red,
                "info": self.term.cyan,
                "terminal": self.term.bright_black,
                "logging_error": self.term.red,
                "logging_warning": self.term.yellow,  # Color for warning messages
                "logging_info": self.term.bright_cyan,  # Color for info messages
                "logging_debug": self.term.bright_green,  # Color for debug messages
                "logging_critical": self.term.magenta  # Color for critical messages
            }
        }
    
    def ui_setup(self,color_schema:str = 'default') -> None:
        """This method sets up the user interface based on the selected color schema.

        :param color_schema: The color schema to apply, defaults to 'default'
        :type color_schema: str, optional

        :raises: None

        :return: None
        :rtype: None
        """
        self.color_schema = self.color_schemas[color_schema]
        if color_schema != 'system':
            self.term.fullscreen()
            self.term.clear()
            os.system('cls')
            self.term.hidden_cursor()
            print( self.term.home + self.color_schema['background'])
    
        else:
            self.term.fullscreen()
            self.term.hidden_cursor()
            print(self.term.clear)
        
        self.calculate_positions()
        self.print_horizontal_separator(width=self.data_max_width+2)
        if self.debug_section and self.term.width > self.debug_min_width:
            self.print_vertical_separator(start_x=self.data_max_width+2)

    def calculate_positions(self) -> None:
        """
        Calculate and set the positions and sizes of UI elements based on the current configuration.

        This method adjusts the positions and sizes of UI elements, such as the data section and debug panel,
        based on the current configuration, including the display mode, terminal width, and debug section settings.
        """
        terminal_width = self.term.width

        self.prompt_start = (self.term.height - 1, 0)
        self.message_start = (self.term.height - 2,0)
        self.interactive_debug_start = (self.term.height - 4,0)


        if self.display_mode in ['normal','dynamic'] and ((self.debug_section and terminal_width < 200) or (not self.debug_section and terminal_width < 160)):
            self.debug_start = (0, 83)
            self.interactive_debug_start = (self.term.height - 4, 0)
            self.data_max_width = 80
            self.debug_min_width = 120
            # Calculate other positions and sizes for normal mode
        elif self.display_mode in ['wide','dynamic']  and ((self.debug_section and terminal_width < 280) or (not self.debug_section and terminal_width < 240)):
            self.debug_start = (0, 163)
            self.interactive_debug_start = (self.term.height - 4, 0)
            self.data_max_width = 160
            self.debug_min_width = 200
            # Calculate other positions and sizes for wide mode
        else:
            self.debug_start = (0, 243)
            self.interactive_debug_start = (self.term.height - 4, 0)
            self.data_max_width = 240
            self.debug_min_width = 280
            # Calculate other positions and sizes for ultrawide mode

    
    def set_prompt(self,prompt:str) -> None:
        """Set the prompt message for the user interface.

        :param prompt: The prompt message to set.
        :type prompt: str

        :raises: None

        :return: None
        :rtype: None
        """
        if self.color_schema:
            self.prompt_message = self.color_schema['prompt'] + prompt + self.term.normal + self.color_schema['background']
        else:
            self.prompt_message = prompt
        
        self.is_prompt_updated = True
            
        self.display()
    
    def set_message(self,message:str,type:str=None) -> None:
        """
        Set a message to display in the user interface.

        :param message: The message to set.
        :type message: str

        :param type: The type of message (optional). Should be 'error' or 'info' if specified.
        :type type: str, optional

        :raises: None

        :return: None
        :rtype: None
        """
        if not type or type not in ('error','info'):
            type = 'message'
        if self.color_schema and type in self.color_schema:
            self.message = self.color_schema[type] + message + self.term.normal + self.color_schema['background']
        else:
            self.message = message
        
        self.is_message_updated = True
        
        self.display()

    def copy_data_to_clipboard(self) -> None:
        """
        Copy the original data content to the clipboard.

        This method joins the lines of text in the original data content into a single string
        separated by newline characters and copies it to the clipboard using the pyperclip library.

        :raises: None

        :return: None
        :rtype: None
        """
        # Join the original data content into a single string
        original_data_text = "\n".join(self.original_data_content)

        # Use pyperclip to copy the original data to the clipboard
        pyperclip.copy(original_data_text)

    
    def add_to_data(self, text: str) -> None:
        """
        Add text to the data content of the user interface.

        :param text: The text to add to the data content.
        :type text: str

        :raises: None

        :return: None
        :rtype: None
        """
        # Store the original text
        self.original_data_content.append(text)

        # Split the input text into lines based on newline characters
        lines = text.split('\n')

        wrapped_lines = []

        # Wrap each line individually based on the specified width
        for line in lines:
            wrapped_lines = wrapped_lines + self.term.wrap(line, width=self.data_max_width)


        # Apply color to each line after wrapping
        colored_wrapped_lines = [line if not self.color_schema else (self.color_schema['text'] + line + self.term.normal) for line in wrapped_lines]

        # Store the formatted text
        self.data_content.extend(colored_wrapped_lines)

        self.is_data_updated = True

        # Display the updated content
        self.display()

    def redraw_data(self) -> None:
        """
        Re-draw the data content based on the current max_data_width.

        :raises: None

        :return: None
        :rtype: None
        """
        # Clear the current data content
        self.data_content = []
        if not self.original_data_table[0]:

            # Re-wrap and re-format the original text using the new max_data_width
            for original_text in self.original_data_content:
                # Wrap the original text to fit within the current max_data_width
                wrapped_text = self.term.wrap(original_text, width=self.data_max_width)

                # Apply color to each line after wrapping
                colored_wrapped_text = [line if not self.color_schema else (self.color_schema['text'] + line + self.term.normal) for line in wrapped_text]

                # Store the formatted text
                self.data_content.extend(colored_wrapped_text)

        else:
            self.build_and_add_table(self.original_data_table[0],self.original_data_table[1],self.original_data_table[2],self.original_data_table[3])
        
        self.is_data_updated = True
    
    def _scroll_up_data(self) -> None:
        """
        Scroll the data content up in the user interface.

        This method is intended for internal use only. It is used to scroll the data content
        upward in the user interface. It checks if there is content above the currently
        displayed content and if there is more content to display. If both conditions are met,
        it moves the `data_top` position up by one, indicating that the data content should be
        scrolled up, and sets the `data_updated` flag to True.

        :raises: None
        
        :return: None
        :rtype: None
        """
        if self.data_top > 0 and len(self.data_content) > self.data_height:
            self.data_top -= 1
            self.is_data_updated = True

            
    def _scroll_down_data(self):
        """
        Scroll the data content down in the user interface.

        This method is intended for internal use only. It calculates the number of rows that can
        be displayed after scrolling down and allows scrolling down only if there are more rows to
        display after the scroll. If conditions are met, it increments the `data_top` position by
        one, indicating that the data content should be scrolled down, and sets the `data_updated`
        flag to True.

        :raises: None

        :return: None
        :rtype: None
        """
        # Calculate the number of rows that can be displayed after scrolling down
        rows_after_scroll = len(self.data_content) - (self.data_top + self.data_height)
        
        # Allow scrolling down only if there are more rows to display after the scroll
        if rows_after_scroll > 0:
            self.data_top += 1
            self.is_data_updated = True
    
    def process_input(self, key:str) -> None:
        """
        Process user input in the user interface.

        :param key: The input key to be processed.
        :type key: str

        :raises: None

        :return: None
        :rtype: None
        """
        if key.code == self.term.KEY_UP:
            self._scroll_up_data()
        elif key.code == self.term.KEY_DOWN:
            self._scroll_down_data()
        elif key.code == self.term.KEY_BACKSPACE:
            self.user_input = self.user_input[:-1] 
            self.is_input_updated = True
        elif key.is_sequence:
            pass
        elif key != '\r' and key != '\n':  # Any other non-special key
            self.user_input += key 
            self.is_input_updated = True 
    
    def print_horizontal_separator(self,width:int=80,start_x:int=0,start_y:int=5) -> None:
        """
        Print a horizontal separator line in the user interface.

        :param width: The width of the separator line, defaults to 80.
        :type width: int, optional

        :param start_x: The starting x-coordinate for the separator line, defaults to 0.
        :type start_x: int, optional

        :param start_y: The starting y-coordinate for the separator line, defaults to 5.
        :type start_y: int, optional

        :raises: None

        :return: None
        :rtype: None
        """
        with self.term.location(start_x,self.term.height - start_y):
            if self.color_schema and 'terminal' in self.color_schema:
                print(self.color_schema['terminal'] + '-' * width) 
            else:
                print('-' * width)
    
    def print_vertical_separator(self, start_x:int=83, start_y:int=0) -> None:
        """
        Print a vertical separator line in the user interface.

        :param start_x: The starting x-coordinate for the separator line.
        :type start_x: int, optional

        :param start_y: The starting y-coordinate for the separator line.
        :type start_y: int, optional

        This method is responsible for printing a vertical separator line in the user interface.
        It takes optional parameters `start_x` and `start_y` to specify the starting coordinates
        for the separator.

        The appearance of the separator line may vary based on the color schema if available.

        :raises: None

        :return: None
        :rtype: None
        """
        for y in range(start_y,self.term.height):
            with self.term.location(start_x, y):
                if self.color_schema and 'terminal' in self.color_schema:
                    print(self.color_schema['terminal'] + '|')
                else:
                    print('|')
    
    def display(self) -> None:  
        """
        Update and display the user interface.

        This method is responsible for updating and displaying the user interface. It checks for
        changes in the data content, message, and input line, and updates them accordingly.

        :raises: None

        :return: None
        :rtype: None
        """
        with self.term.cbreak():
            if self.term.width != self.prev_terminal_width:
                self.calculate_positions()
                self.prev_terminal_width = self.term.width
                self.term.clear()
                os.system('cls')
                self.print_horizontal_separator(width=self.data_max_width+2)
                if self.debug_section and self.term.width > self.debug_min_width:
                    self.print_vertical_separator(start_x=self.data_max_width+2)
                self.redraw_data()
                self.debug_update = True
                self.is_message_updated = True
                self.is_prompt_updated = True


            # Update data content if it has changed
            if self.is_data_updated:
                for i, line in enumerate(self.data_content[self.data_top:self.data_top + self.data_height]):
                    print(self.term.move_xy(self.data_start[1], i + self.data_start[0]) + ' '*self.data_max_width)
                    print(self.term.move_xy(self.data_start[1], i + self.data_start[0]) + line)
                self.is_data_updated = False

            # Update the message if it has changed
            if self.is_message_updated:
                message_to_clear = self.message[:self.data_max_width]  # Get the message up to max_data_length
                clear_padding = ' ' * (self.data_max_width - len(message_to_clear))  # Calculate padding
                print(self.term.move_xy(self.message_start[1], self.message_start[0]) + message_to_clear + clear_padding)
                self.is_message_updated = False

            # Update the input line if it has changed
            if self.is_prompt_updated or self.is_input_updated:
                input_line_position = self.term.height - 1
                message_to_clear = f"{self.prompt_message} {self.user_input}"[:self.data_max_width]   # Get the message up to max_data_length
                clear_padding = ' ' * (self.data_max_width - len(message_to_clear))  # Calculate padding
                print(self.term.move_xy(self.message_start[1], self.message_start[0]) +  message_to_clear + clear_padding)
                self.is_input_updated = False
                self.is_prompt_updated = False
            
            self.display_debug_section()
        
    
    def run(self, wait_for_key:bool=True) -> None:
        """
        Run the user interface loop.

        This method runs the main loop of the user interface, continuously displaying and updating
        the interface. It waits for user input and processes it accordingly.

        :param wait_for_key: Whether to wait for a key press to exit the loop (default is True).
        :type wait_for_key: bool, optional

        :raises: None

        :return: None
        :rtype: None
        """
        with self.term.cbreak():
            while True:
                self.display()
                key = self.term.inkey(timeout=0.1)
                if wait_for_key:
                    if key == 'c':
                        self.copy_data_to_clipboard()
                    elif key == '\r' or key == '\n':
                        break
                    elif key.code == self.term.KEY_UP:
                        self._scroll_up_data()
                    elif key.code == self.term.KEY_DOWN:
                        self._scroll_down_data()
                elif key:
                    self.process_input(key)
                    if key == '\r' or key == '\n':  # Check for Enter key
                        break  # Exit the run loop

    def exit(self) -> None:
        """
        Clean up and exit the user interface.

        This method performs cleanup tasks and reverts any changes made to the terminal, such as
        clearing the screen and resetting the terminal state. It is called when exiting the user interface.

        :raises: None

        :return: None
        :rtype: None
        """
        # Revert any changes made to the terminal (like fullscreen, hidden cursor)
        print(self.term.normal)  # Resets the terminal state
        print(self.term.clear)   # Clears the terminal screen
        
    def build_and_add_table(self, data: List, max_widths: List[Optional[int]] = None, max_table_width: int = None, min_column_width: int = 10) -> None:
        """
        Build and add a table to the user interface.

        This method constructs a table from the given data and adds it to the user interface's data content.
        It calculates column widths, adjusts them if necessary to fit within the specified table width,
        and formats the header, rows, and footer of the table.

        :param data: The data to be displayed in the table, where each row is a list of cell values.
        :type data: List[List[Union[str, int]]]

        :param max_widths: Optional list of maximum column widths. Use None for columns without maximum width constraints.
        :type max_widths: List[Optional[int]], optional

        :param max_table_width: The maximum width of the table, defaults to 80.
        :type max_table_width: int, optional

        :param min_column_width: The minimum width for each column, defaults to 10.
        :type min_column_width: int, optional

        :raises: None

        :return: None
        :rtype: None
        """
        self.original_data_table = (data, max_widths, max_table_width, min_column_width)

        if not max_table_width:
            max_table_width = self.data_max_width

        num_columns = len(data[0])
        column_widths = [0] * num_columns

        # Calculate natural width for each column
        for row in data:
            for i, cell in enumerate(row):
                column_widths[i] = max(column_widths[i], len(str(cell)))

        # Apply max widths if specified
        for i in range(num_columns):
            if max_widths and i < len(max_widths) and max_widths[i] is not None:
                column_widths[i] = min(column_widths[i], max_widths[i])

        # Adjust widths if total width is not optimal
        total_width = sum(column_widths) + (2 * num_columns) + (num_columns - 1) + 2
        if total_width != max_table_width:
            self.adjust_column_widths(column_widths, max_widths, total_width, max_table_width, min_column_width)

        # Build and add the table header
        header_line = self.format_table_header_footer(column_widths)
        self.data_content.append(header_line)

        # Insert the header row and separator line
        header_row = self.format_table_header(data[0], column_widths)
        self.data_content.append(header_row)
        separator_line = self.format_table_header_footer(column_widths)
        self.data_content.append(separator_line)

        # Build and add the rest of the table rows
        for row in data[1:]:  # Skip the first row as it's the header
            formatted_row = self.format_table_row(row, column_widths)
            self.data_content.append(formatted_row)

        # Build and add the table footer
        footer_line = self.format_table_header_footer(column_widths)
        self.data_content.append(footer_line)

        self.is_data_updated = True
        self.display()

    def adjust_column_widths(self, widths: List[int], max_widths: Optional[List[Optional[int]]], total_width: int, max_table_width: int, min_column_width: int) -> None:
        """
        Adjust column widths to fit within the specified table width.

        This method adjusts the column widths to fit within the specified maximum table width while
        respecting maximum and minimum width constraints for individual columns.

        :param widths: List of current column widths to be adjusted.
        :type widths: List[int]

        :param max_widths: Optional list of maximum column widths. Use None for columns without maximum width constraints.
        :type max_widths: List[Optional[int]], optional

        :param total_width: The current total width of the table, including column widths and separators.
        :type total_width: int

        :param max_table_width: The maximum width of the table.
        :type max_table_width: int

        :param min_column_width: The minimum width for each column.
        :type min_column_width: int

        :raises: None

        :return: None
        :rtype: None
        """
        difference = max_table_width - total_width
        flexible_columns = [i for i in range(len(widths)) if not max_widths or i >= len(max_widths) or max_widths[i] is None]

        while difference != 0 and flexible_columns:
            for i in flexible_columns:
                # Increase width if needed and no max width constraint or under max width
                if difference > 0 and (not max_widths or i >= len(max_widths) or max_widths[i] is None or widths[i] < max_widths[i]):
                    widths[i] += 1
                    difference -= 1
                # Decrease width if needed and above min width
                elif difference < 0 and widths[i] > min_column_width:
                    widths[i] -= 1
                    difference += 1
                if difference == 0:
                    break


    def format_table_row(self, row: List[Union[str, int]], widths: List[int]) -> str:
        """
        Format a table row with colored elements and text.

        This method takes a row of data and a list of column widths, and it formats the row with colors for elements
        and text alignment.

        :param row: The data for the row, where each element can be a string or an integer.
        :type row: List[Union[str, int]]

        :param widths: The list of column widths corresponding to each element in the row.
        :type widths: List[int]

        :returns: The formatted row with color styling.
        :rtype: str
        """
        # Format the table row with color for elements and text
        if self.color_schema:
            # Apply color formatting if color schema is available
            colored_cells = []
            for cell, width in zip(row, widths):
                if len(str(cell)) > width:
                    # Truncate cell content if it exceeds the width
                    cell = str(cell)[:width]
                formatted_cell = (
                    f"{self.color_schema['table_output']}|{self.color_schema['table_elements']} "
                    f"{str(cell).ljust(width)}{self.color_schema['table_output']}"
                )
                colored_cells.append(formatted_cell)
            colored_row = ' '.join(colored_cells)
            return colored_row + self.color_schema['table_output'] + ' |'
        else:
            # Return plain formatted row if color schema is not available
            plain_cells = []
            for cell, width in zip(row, widths):
                formatted_cell = str(cell).ljust(width)
                plain_cells.append(formatted_cell)
            return ' | '.join(plain_cells)


    def format_table_header(self, header: List[Union[str, int]], widths: List[int]) -> str:
        """
        Format a table header with centered elements and color (if color schema is provided).

        This method takes a header row and a list of column widths, and it formats the header with centered elements
        and color if a color schema is provided.

        :param header: The header row containing column labels or titles.
        :type header: List[Union[str, int]]

        :param widths: The list of column widths corresponding to each element in the header.
        :type widths: List[int]

        :returns: The formatted header with centered elements and color styling (if color schema is available),
                  or a plain formatted header.
        :rtype: str
        """
        if self.color_schema:
            # Apply color formatting and center-align header elements if color schema is available
            centered_header_elements = [
                f"{self.color_schema['table_output']}|{self.color_schema['table_elements']} {str(cell).center(width)} "
                for cell, width in zip(header, widths)
            ]
            centered_header = ''.join(centered_header_elements) + self.color_schema['table_output'] + '|'
            return centered_header
        else:
            # Return plain formatted header with centered elements if color schema is not available
            return ' | '.join(str(cell).center(width) for cell, width in zip(header, widths))

    def format_table_header_footer(self, widths: List[int]) -> str:
        """
        Format a table header/footer line with color (if color schema is provided).

        This method creates a header/footer line for the table with color styling if a color schema is provided.

        :param widths: The list of column widths corresponding to each element in the header/footer line.
        :type widths: List[int]

        :returns: The formatted header/footer line with color styling (if color schema is available),
                  or a plain formatted line.
        :rtype: str
        """
        if self.color_schema:
            # Apply color formatting to the header/footer line if color schema is available
            line_elements = [
                '-' * (w + 2) for w in widths  # +2 for padding around text
            ]
            line = f"+{'+'.join(line_elements)}+"
            return f"{self.color_schema['table_output']}{line}"
        else:
            # Return plain formatted header/footer line if color schema is not available
            return '+' + '+'.join('-' * (w + 2) for w in widths) + '+'
            
    def add_debug_message(self, message: str) -> None:
        """
        Add a debug message to the list of debug messages.

        This method inserts a new debug message at the beginning of the list of debug messages.

        :param message: The debug message to be added.
        :type message: str

        :return: None
        :rtype: None
        """
        # Insert new message at the beginning of the list
        self.debug_messages.insert(0, message)
        self.debug_update = True
        self.display_debug_section()
        

    def display_debug_section(self) -> None:
        """
        Display the debug section of the user interface.

        This method is responsible for displaying the debug section on the terminal. It checks for changes in the
        debug messages and updates the display accordingly.

        :raises: None

        :return: None
        :rtype: None
        """
        if self.debug_section and self.term.width > 120:
            available_width = self.term.width - self.debug_start[1]  # Use column directly from self.debug_start

            display_buffer: List[str] = []

            for message in reversed(self.debug_messages):
                log_level = message.split(' - ')[1]
                color = self.get_log_color(log_level)
                print_parts = message.split(' - ')[1:]
                print_message = ' - '.join(print_parts)
                wrapped_message = self.term.wrap(print_message, width=available_width)

                # Apply color to each line after wrapping
                colored_wrapped_message = [color + line + self.term.normal for line in wrapped_message]

                # Prepend lines to the display buffer
                display_buffer = colored_wrapped_message + display_buffer
                if len(display_buffer) >= self.max_debug_lines:
                    display_buffer = display_buffer[-self.max_debug_lines:]
                    break

            for i, line in enumerate(display_buffer):
                with self.term.location(self.debug_start[1], self.debug_start[0] + i):  # Use row and column from self.debug_start
                    # Clear the line
                    print(' ' * available_width, end='')
                    # Move cursor back to start of line and print the line
                    print(self.term.move_x(self.debug_start[1]) + line)
        else:
            # Display latest message at a specific position
            with self.term.location(self.interactive_debug_start[1], self.interactive_debug_start[0]):
                log_level = self.debug_messages[0].split(' - ')[1]
                color = self.get_log_color(log_level)
                print_parts = self.debug_messages[0].split(' - ')[1:]
                print_message = ' - '.join(print_parts)
                print(color + print_message + self.term.normal)
        self.debug_update = False  # Reset the update flag
                    

    def get_log_color(self, log_level: str) -> str:
        """
        Get the color code based on the log level.

        This method returns the color code based on the provided log level. If the color schema is available in the
        instance, it looks up the color based on the log level key. If not found, it defaults to the terminal's
        normal color.

        :param log_level: The log level for which to determine the color.
        :type log_level: str

        :return: The color code.
        :rtype: str
        """
        if hasattr(self, 'color_schema'):
            color_key = f'logging_{log_level.lower()}'
            return self.color_schema.get(color_key, self.term.normal)  # Default to normal if not found
        return self.term.normal

    def clear_debug_section(self) -> None:
        """
        Clear the debug section on the screen.

        This method clears the debug section, assuming it starts at a specific column and spans a certain width and height.

        :raises: None

        :return: None
        :rtype: None
        """
        debug_section_start_col = self.debug_start[1]
        debug_section_width = self.term.width - debug_section_start_col
        debug_section_height = self.max_debug_lines  # Assuming max_debug_lines represents the height

        # Clear each line in the debug section
        for i in range(debug_section_height):
            with self.term.location(debug_section_start_col, i):
                print(' ' * debug_section_width, end='')
        
    
    