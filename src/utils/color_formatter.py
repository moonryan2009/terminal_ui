import logging
from blessed import Terminal
from typing import List, Union

class ColorFormatter:
    @staticmethod
    def apply_color(term:Terminal, text:str, color: Union[str,int], background=False) -> str:
        """Applies specified color to the given text using the Blessed Terminal.

        This method handles both named and numeric color formats, with support for
        both foreground and background coloring.

        :param term: A Blessed Terminal instance used for applying color.
        :param text: The text to which color formatting will be applied.
        :param color: The color to apply, which can be a string (for named colors)
                      or an integer (for 256-color mode).
        :param background: Flag to indicate if the color is to be applied as a
                           background color. Defaults to False (foreground color).
        :type term: Terminal
        :type text: str
        :type color: Union[str, int]
        :type background: bool

        :return: The text with the applied color formatting.
        :rtype: str

        If the color is a string, the method checks if it's a named attribute in
        the Blessed Terminal; if not, it attempts to interpret it as a number for
        256-color mode. If the color is an integer, it's directly used in 256-color mode.
        The method returns the original text if no valid color formatting is applicable.
        """
        try:
            if isinstance(color, str):
                color = color.lower()
                if hasattr(term, color if not background else 'on_' + color):
                    return getattr(term, color if not background else 'on_' + color)(text)
                elif color.isdigit():
                    return term.color(int(color))(text) if not background else term.on_color(int(color))(text)
            elif isinstance(color, int):
                return term.color(color)(text) if not background else term.on_color(color)(text)
            return text
        except Exception as e:
            logging.error(f'Unable to apply color {e}')
            return text