
from typing import List, Union, Optional
from blessed import Terminal

class Hotkey:
    def __init__(self, key_str:str=None, function:Union[callable,str]=None, label:str=None):
        self.key_str = key_str
        self.function = function
        self.key_code = self.get_key_code(key_str)
        self.label = label

    @staticmethod
    def get_key_code(key_combination) -> str:
        """
        Converts a key combination string to its corresponding key code using blessed Terminal.

        :param key_combination: Key combination string (e.g., 'ctrl+n', 'f1').
        :return: The corresponding key code or control character.
        """
        term = Terminal()
        key_combination = key_combination.lower()

        # Handling Function Keys Dynamically
        if key_combination.startswith('f') and key_combination[1:].isdigit():
            func_key_name = f'KEY_F{key_combination[1:]}'
            func_key_code = getattr(term, func_key_name, None)
            if func_key_code is not None:
                return func_key_code

        # Handling Control Key Combinations
        if key_combination.startswith('ctrl+'):
            letter = key_combination.split('+')[1].upper()
            if len(letter) == 1 and 'A' <= letter <= 'Z':
                return chr(ord(letter) - 64)  # Convert letter to control character

        raise ValueError(f"Invalid key combination: {key_combination}")
        

    def __str__(self):
        # Get a readable name for the function
        function_name = self.function.__name__ if callable(self.function) else str(self.function)
        label = self.label if self.label else function_name
        return f"{self.key_str}:{label}"