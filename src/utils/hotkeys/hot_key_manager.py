import logging
from typing import List, Union, Optional
from src.utils.hotkeys.hot_key import Hotkey

class HotkeyManager:

    def __init__(self):
        self.hotkeys = {}
        self.hotkey_string = ''
        
    def add_hotkey(self, key_str: str = None, function: Union[callable, str] = None, label: str = None) -> None:
        """
        Adds a new hotkey to the Terminal UI.
        
        :param key_str: The string representation of the hotkey (e.g., 'ctrl+n', 'f1').
        :type key_str: str
        :param function: The function to be executed when the hotkey is pressed.
        :type function: Union[callable, str]
        :param label: The label for the HotKey (i.e. Help or Settings)
        :type label: str

        :raises ValueError: If the hotkey string or function is missing or invalid, or if the hotkey is already in use.
        """
        if not key_str or not function:
            raise ValueError("Key string and function are required to add a hotkey.")

        try:
            hotkey = Hotkey(key_str, function, label)
            if hotkey.key_code in self.hotkeys:
                raise ValueError(f"Hotkey '{key_str}' is already in use.")

            self.hotkeys[hotkey.key_code] = hotkey
            function_name = function if isinstance(function, str) else getattr(function, '__name__', 'Unnamed Function')
            logging.debug(f'Successfully added hotkey {key_str} with action {label or function_name}')
            
            self.hotkey_string = self.format_hotkey_info()
        except ValueError as e:
            logging.error(f"Failed to add hotkey: {e}")
            raise

    def remove_hotkey(self, hotkey: str = None) -> None:
        """
        Removes a hotkey mapping.

        :param hotkey: The hotkey to be removed.
        """
        if not hotkey:
            raise ValueError("Hotkey string value must be provided")
        if hotkey not in self.hotkeys:
            raise ValueError(f"Hotkey '{hotkey}' is not mapped and cannot be removed.")
        del self.hotkeys[hotkey]

        logging.debug(f'Successfully removed: {hotkey}')

    def handle_hotkey(self, key_code):
        """
        Handles actions associated with a hotkey.

        :param key_code: The key code of the pressed key.
        :type key_code: int
        """
        if key_code in self.hotkeys:
            hotkey = self.hotkeys[key_code]
            action = hotkey.function
            if callable(action):
                action()
            elif isinstance(action, str):
                self.handle_menu_action(action)
            else:
                logging.warning(f"Unhandled action type for hotkey: {key_code}")

    def format_hotkey_info(self):
        """
        Formats the hotkey information for display in the help panel.
        """
        hotkey_strings = [str(hotkey) for hotkey in self.hotkeys.values()]

        # Custom sorting logic
        def sort_key(s):
            # Separate F keys, Ctrl keys, and others
            if s.lower().startswith('f'):
                # Extract the number part of the F key and convert to integer for correct numerical sorting
                # Ensure that non-digit parts are handled correctly (e.g., F12 vs F1)
                f_key_number = int(''.join(filter(str.isdigit, s)))
                return (0, f_key_number)  # F keys sorted by their number
            elif s.lower().startswith('ctrl+'):
                return (1, s)  # Ctrl keys
            else:
                return (2, s)  # Other keys



        hotkey_strings.sort(key=sort_key)

        return ' | '.join(hotkey_strings)
    
    