import logging
from src.utils.log_handler import UILogHandler

import logging

def setup_logging(filename:str=None, level=logging.DEBUG, debug_panel=None,ui=None):
    # Configure the logging level and format
    logging.basicConfig(level=level, 
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        filename=filename,  # Log to a file
                        filemode='a')  # Append mode

    # File handler for logging to a file
    if filename:
        file_handler = logging.FileHandler(filename)
        file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(file_formatter)
        logging.getLogger('').addHandler(file_handler)


    



