class DataStore:
    """
    A simple class for storing and retrieving data.

    Attributes:
        data (dict): A dictionary to store data items.
    """

    def __init__(self):
        """
        Initializes the DataStore with an empty dictionary.
        """
        self.data = {}

    def store(self, key, value):
        """
        Store a value with a specified key in the data store.

        :param key: The key under which the value is stored.
        :param value: The value to be stored.
        :type key: str
        """
        self.data[key] = value

    def retrieve(self, key):
        """
        Retrieve a value from the data store by its key.

        :param key: The key of the value to be retrieved.
        :type key: str
        :return: The value associated with the key, or None if the key does not exist.
        """
        return self.data.get(key,None)

    def delete(self, key):
        """
        Delete a value from the data store by its key.

        :param key: The key of the value to be deleted.
        :type key: str
        """
        if key in self.data:
            del self.data[key]