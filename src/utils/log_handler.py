import logging

class UILogHandler(logging.Handler):
    def __init__(self, ui = None, panel: str = 'debug_ui', max_severity=logging.DEBUG):
        super().__init__()
        self.ui = ui
        self.panel = panel
        self.max_severity = max_severity
        self.setFormatter(logging.Formatter('%(levelname)s - %(message)s'))

    def emit(self, record):
        try:
            #if record.levelno >= self.max_severity:
            if self.ui and self.panel in self.ui.panel_manager.panels:
                message = self.format(record)
                self.ui.panel_manager.panels[self.panel].add_data(message, update_type='append')
            else:
                print("UI not available for logging:", self.format(record))
        except Exception as e:
            print("Error while logging to UI panel:", e)