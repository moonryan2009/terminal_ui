import logging
from blessed import Terminal
from typing import List, Union, Optional

from src.messages.text_message import TextMessage
from src.messages.table_message import TableMessage

from src.utils.color_formatter import ColorFormatter

class DisplayPanel:
    def __init__(self, name: str, term: Terminal, term_height: int = None, panel_x: int = None, panel_y: int = None,
                 width: int = None, height: int = None, fg_color: str = None, term_width: int = None,
                 bg_color: str = None, data_padding_y: int = 1, term_bg_color=None,
                 data_padding_x: int = 2, outline_top: bool = False,
                 outline_right: bool = False, outline_bottom: bool = False,
                 outline_left: bool = False, outline_horizontal_char: str = '-',
                 outline_vertical_char: str = '|', outline_color: Union[str, int] = None,
                 outline_bg: Union[str, int] = None, outline_highlight_bg: Union[str, int] = None,
                 outline_highlight: bool = False, outline_original: dict = None,
                 dimension_config: Union[str, dict] = None,
                 offset_from_left: int = None, offset_to_right: int = None,               
                 offset_from_top: int = None, offset_to_bottom: int = None,
                 offset_from_bottom: int = None, panel_hidden: bool = False,
                 offset_from_right: int = None, expanded_direction:str=None,
                 expanded_size:int=None):
        """
        Initialize the DisplayPanel with customizable properties for display in a terminal.

        :param name: Name identifier for the panel.
        :param term: Terminal object for rendering.
        :param term_height: Terminal height, defaults to terminal's current height.
        :param panel_x: X-coordinate for panel's position, defaults to 0.
        :param panel_y: Y-coordinate for panel's position, defaults to 0.
        :param width: Width of the panel, dynamic if None.
        :param height: Height of the panel, dynamic if None.
        :param fg_color: Foreground color for text.
        :param term_width: Terminal width, defaults to terminal's current width.
        :param bg_color: Background color for the panel.
        :param data_padding_y: Vertical padding, defaults to 1.
        :param term_bg_color: Background color for terminal, defaults to bg_color.
        :param data_padding_x: Horizontal padding, defaults to 2.
        :param outline_top: If True, display outline at the top.
        :param outline_right: If True, display outline on the right.
        :param outline_bottom: If True, display outline at the bottom.
        :param outline_left: If True, display outline on the left.
        :param outline_horizontal_char: Character for horizontal outline.
        :param outline_vertical_char: Character for vertical outline.
        :param outline_color: Color for the outline.
        :param outline_bg: Background color for the outline, defaults to panel's bg_color.
        :param outline_highlight_bg: Highlight background color for outline.
        :param outline_highlight: If True, enable outline highlighting.
        :param outline_original: Dictionary to store original outline settings.
        :param dimension_config: Preset or custom dimension configuration.
        :param offset_from_left: Offset from the left, used if width is dynamic.
        :param offset_to_right: Offset to the right, used if width is dynamic.
        :param offset_from_top: Offset from the top, used if height is dynamic.
        :param offset_to_bottom: Offset to the bottom, used if height is dynamic.
        :param offset_from_bottom: Offset from the bottom, used if height is dynamic.
        :param panel_hidden: If True, the panel is not displayed.
        :param offset_from_right: Offset from the right, used if width is dynamic.
        """
        self.name = name
        self.term = term
        self.term_height = term_height or term.height
        self.term_width = term_width or term.width
        self.term_bg_color = term_bg_color or bg_color
        self.panel_hidden = panel_hidden
        self.panel_x = panel_x #recalculated in calculate_starting_x if None
        self.panel_y = panel_y #recalculated in calculate_starting_y if None
        self.panel_width = width
        self.panel_height = height
        self.offset_from_top = offset_from_top
        self.offset_to_bottom = offset_to_bottom
        self.offset_from_bottom = offset_from_bottom
        self.offset_from_left = offset_from_left
        self.offset_to_right = offset_to_right
        self.offset_from_right = offset_from_right
        self.panel_height_calculated = height is None
        self.panel_width_calculated = width is None
        self.dimension_config = dimension_config or {}
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.data_padding_x = data_padding_x
        self.data_padding_y = data_padding_y
        self.data_top = 0
        self.data = []
        self.formatted_lines = []
        self.outline_top = outline_top
        self.outline_right = outline_right
        self.outline_bottom = outline_bottom
        self.outline_left = outline_left
        self.outline_horizontal_char = outline_horizontal_char
        self.outline_vertical_char = outline_vertical_char
        self.outline_color = outline_color
        self.outline_bg = outline_bg or self.bg_color
        self.outline_highlight_bg = outline_highlight_bg or self.outline_bg
        self.outline_highlight = outline_highlight
        self.outline_original = outline_original or {}
        
        self.expanded_size = expanded_size
        self.expanded_direction = expanded_direction

        # Initialize empty dimension dictionary if not provided
        self.empty_dimension = {
            'panel_x': None,
            'panel_y': None,
            'panel_width': None,
            'panel_height': None,
            'panel_height_calculated': None,
            'panel_width_calculated': None,
            'offset_to_bottom': None,
            'offset_from_bottom': None,
            'offset_from_left': None,
            'offset_from_right': None,
            'offset_to_right': None
        }

        # Store original dimensions if not already saved
        if 'original' not in self.dimension_config:
            self.dimension_config['original'] = {
                'panel_x': self.panel_x,
                'panel_y': self.panel_y,
                'panel_width': self.panel_width,
                'panel_height': self.panel_height,
                'panel_height_calculated': self.panel_height_calculated,
                'panel_width_calculated': self.panel_width_calculated,
                'offset_to_bottom': self.offset_to_bottom,
                'offset_from_bottom': self.offset_from_bottom,
                'offset_from_left': self.offset_from_left,
                'offset_from_right': self.offset_from_right,
                'offset_to_right': self.offset_to_right
            }

        # Calculate and initialize panel dimensions and settings
        self.panel_height = self._calculate_height()
        self.panel_width = self._calculate_width()
        self.panel_x = self.calculate_starting_x()
        self.panel_y = self.calculate_starting_y()
        self.calculate_horizontal_text_padding()
        self.calculate_vertical_text_padding()
        self.clear_panel()
        self.init_panel()

    
    def resize_panel(self, setting_name: str = None, term_height: int = None,
                      term_width: int = None, panel_width: int = None,
                      panel_height: int = None, redraw_panel=True):
        """
        Resizes the panel based on a specified configuration.

        :param setting_name: The name of the configuration setting to apply. If None or not found, the method does nothing.
        :param term_height: Optional new terminal height.
        :param term_width: Optional new terminal width.
        :param panel_width: Optional new panel width.
        """
        self.clear_panel()

        self._update_terminal_dimensions(term_height, term_width)
        self.panel_width = panel_width or self.panel_width
        self.panel_height = panel_height or self.panel_height

        if setting_name and setting_name in self.dimension_config:
            self._apply_dimension_config(setting_name)

        self._recalculate_dimensions()
        self._update_formatted_lines()
        if redraw_panel:
            self._reinitialize_panel()

    def _update_terminal_dimensions(self, term_height: int, term_width: int):
        """Update the terminal dimensions."""
        self.term_height = term_height or self.term.height
        self.term_width = term_width or self.term.width

    def _apply_dimension_config(self, setting_name: str):
        """Apply dimensions from the configuration."""
        for key, value in self.dimension_config[setting_name].items():
            setattr(self, key, value)

    def _recalculate_dimensions(self):
        """Recalculate dimensions and padding."""
        self.panel_height = self._calculate_height()
        self.panel_width = self._calculate_width()
        self.panel_x = self.calculate_starting_x()
        self.panel_y = self.calculate_starting_y()
        self.calculate_horizontal_text_padding()
        self.calculate_vertical_text_padding()

    def _reinitialize_panel(self):
        """Reinitialize the panel with updated settings."""
        self.clear_panel()
        self._update_formatted_lines()
        self.init_panel()

    def _update_formatted_lines(self):
        """Update the formatted lines based on the new dimensions."""
        self.formatted_lines = []
        for line in self.data:
            line.max_width = self.data_width
            self.formatted_lines.extend(line.format_message())

    def _calculate_height(self) -> int:
        if self.panel_height is not None and self.panel_height_calculated == False:
            return max(self.panel_height, 0)

        top_offset = self.offset_from_top if self.offset_from_top is not None else 0
        bottom_offset = self.offset_to_bottom if self.offset_to_bottom is not None else 0
        
        # Check for invalid offset configuration
        if self.offset_to_bottom is not None and (self.term_height - top_offset - bottom_offset < self.offset_to_bottom):
            raise ValueError("Invalid offsets: combined top and bottom offsets exceed terminal height or offset_to_bottom")

        if self.offset_to_bottom and self.offset_from_bottom:
            return max(self.offset_from_bottom - self.offset_to_bottom,0)
        
        if self.offset_from_bottom and not self.offset_to_bottom:
            return self.offset_from_bottom
        
        calculated_height = self.term_height - top_offset - bottom_offset
        return max(calculated_height, 0)


    def _calculate_width(self) -> int:
        """Calculates the width of the panel based on the specified width or offset from the right.

        :raises ValueError: If neither width nor offset_from_width is provided.
        """
        if self.panel_width is not None and self.panel_width_calculated == False:
            return max(self.panel_width, 0)
        
        if self.offset_to_right and self.offset_from_right: 
            if self.offset_to_right > self.offset_from_right:
                raise ValueError("Invalid offsets: offset from bottom is larger than offset to bottom")

        if self.offset_from_left and self.offset_from_right: 
            raise ValueError("Invalid offsets: both offset_from_rigth and offset_from_left are defined")

        # Calculate width based on offsets
        left_offset = self.offset_from_left if self.offset_from_left is not None else 0
        right_offset = self.offset_to_right if self.offset_to_right is not None else 0
        right_start = self.offset_from_right if self.offset_from_right is not None and self.offset_from_right > 0 else self.term_width

        calculated_width = right_start - left_offset - right_offset
        return max(calculated_width, 0)  # Ensure width is not negative

    def calculate_starting_x(self) -> int:
        """
        Calculates the starting x-coordinate for the panel based on various offset parameters.
        """
        if self.panel_x is not None:
            return self.panel_x
        
        if self.offset_from_right:
            return self.term_width -  self.offset_from_right

        # Default to leftmost position if offset not provided
        return self.offset_from_left if self.offset_from_left is not None else 0
        

    def calculate_starting_y(self) -> int:
        """
        Calculates the starting y-coordinate for the panel based on the top and bottom offsets, or a direct y-coordinate.

        :raises ValueError: If none of the required parameters for calculating y-coordinate are provided.
        """
        if self.offset_from_top is not None:
            # Use offset from the top
            return self.offset_from_top

        if self.offset_from_top is not None:
            return self.offset_from_top

        if self.offset_from_bottom is not None:
            # Calculate starting y-coordinate based only on the offset from the bottom
            return self.term_height - self.offset_from_bottom

        # Default to starting at the top if no offsets are provided
        return 0
    
    def print_borders(self, highlight_selection:bool=False, highlight_selection_bg:str=None) -> None:
        self.outline_highlight = highlight_selection
        self.print_horizontal_borders(highlight_selection_bg=highlight_selection_bg)
        self.print_vertical_borders(highlight_selection_bg=highlight_selection_bg)
    
    def print_horizontal_borders(self,highlight_selection_bg=None) -> None:
        """Prints the horizontal borders of the panel.

        This method handles the printing of the top and bottom borders of the panel, if they are enabled.
        It uses the specified outline character and colors to create the border. The border is printed at
        the top and bottom edges of the panel, based on the panel's dimensions and position.
        """
        # Choose the background color based on the provided parameters
        if highlight_selection_bg is not None and self.outline_highlight:
            bg_color = highlight_selection_bg
        else:
            bg_color = self.outline_highlight_bg if self.outline_highlight else self.outline_bg
        horizontal_char_top = self.outline_horizontal_char if self.outline_top else ' '
        horizontal_char_bottom = self.outline_horizontal_char if self.outline_bottom else ' '
            
        with self.term.location(self.panel_x,self.panel_y):
            top_border = horizontal_char_top * self.panel_width
            formatted_border = ColorFormatter.apply_color(self.term,top_border,self.outline_color)
            formatted_border = ColorFormatter.apply_color(self.term,formatted_border,bg_color, background=True)
            print(formatted_border, end="")


        with self.term.location(self.panel_x, self.panel_y + self.panel_height - 1):
            bottom_border =  horizontal_char_bottom * self.panel_width
            formatted_border = ColorFormatter.apply_color(self.term,bottom_border,self.outline_color)
            formatted_border = ColorFormatter.apply_color(self.term,formatted_border,bg_color, background=True)
            print(formatted_border, end="")

    
    def print_vertical_borders(self,highlight_selection_bg=None) -> None:
        """
        Prints the vertical borders of the panel.

        This method handles the printing of the left and right vertical borders of the panel,
        if they are enabled. It iterates through each row of the panel's height and prints the
        border characters at the specified positions. The border characters are styled with the
        specified outline color and background color.
        """
        if highlight_selection_bg is not None:
            bg_color = highlight_selection_bg
        else:
            bg_color= self.outline_highlight_bg if self.outline_highlight else self.outline_bg
        vertical_char_left = self.outline_vertical_char if self.outline_left else ' '
        vertical_char_right = self.outline_vertical_char if self.outline_right else ' '
        for row in range(2, self.panel_height):
                with self.term.location(self.panel_x, self.panel_y + row -1):
                    formatted_char = ColorFormatter.apply_color(self.term,vertical_char_left,self.outline_color)
                    formatted_char = ColorFormatter.apply_color(self.term,formatted_char,bg_color, background=True)
                    print(formatted_char, end="")
                    
                if self.panel_width > 1:
                    with self.term.location(self.panel_x + self.panel_width-1, self.panel_y + row -1):
                        formatted_char = ColorFormatter.apply_color(self.term,vertical_char_right,self.outline_color)
                        formatted_char = ColorFormatter.apply_color(self.term,formatted_char,bg_color, background=True)
                        print(formatted_char, end="")
                           
    def calculate_horizontal_text_padding(self) -> None:
        """Calculates the horizontal text padding for the panel.

        This method adjusts the data width and X position (data_x) of the panel's content,
        taking into account the outlines and padding specified. It ensures that the content
        is properly aligned and fits within the panel, considering any outlines and padding.
        """
        self.data_width = self.panel_width
        self.data_x = self.panel_x
        if self.outline_right:
            self.data_width -= 1
        if self.outline_left:
            self.data_width -= 1
            self.data_x += 1
        if self.data_padding_x > 0:
            self.data_x += self.data_padding_x
            self.data_width -= self.data_padding_x * 2

    def calculate_vertical_text_padding(self) -> None:
        """Calculates the vertical text padding for the panel.

        This method adjusts the data height and Y position (data_y) of the panel's content,
        taking into account any top and bottom outlines and padding specified. It ensures that
        the content is properly aligned vertically within the panel, considering any outlines
        and vertical padding.
        """
        self.data_height = self.panel_height
        self.data_y = self.panel_y
        if self.outline_top:
            self.data_height -= 1
            self.data_y += 1
        if self.outline_bottom:
            self.data_height -= 1
        if self.data_padding_y > 0:
            self.data_y += self.data_padding_y
            self.data_height -= self.data_padding_y * 2

    def add_data(self, data: Union[str, List[str]], update_type:str="replace", fg_color:str=None, bg_color:str=None,format:List[str]=None, alignment:str=None) ->None:
        """Adds or updates data in the panel according to the specified update type.

        :param data: Data to be added or displayed. Can be a single string or a list of strings.
        :param update_type: Type of update ('replace', 'append').
        :param fg_color: Optional foreground color for the data.
        :param bg_color: Optional background color for the data.
        :param format: Optional formatting options for the data.
        :type data: str or List[str]
        :type update_type: str
        :type fg_color: str, optional
        :type bg_color: str, optional
        :type format: List[str], optional
        """
        fg_color = fg_color if fg_color is not None else self.fg_color
        bg_color = bg_color if bg_color is not None else self.bg_color
        # Normalize data to a list
        data_list = data if isinstance(data, list) else [data]

        # Create TextMessage objects
        message_objects = [TextMessage(self.term, line, self.data_width, fg_color, 
                                    bg_color, format, alignment) for line in data_list]


        # Format the messages
        for msg in message_objects:
            formatted_lines = msg.format_message() 

        # Update the data based on the specified update type
        if update_type == 'replace':
            self.data = message_objects
            self.formatted_lines = formatted_lines
        elif update_type == 'append':
            self.data.extend(message_objects)
            self.formatted_lines.extend(formatted_lines)

    def add_table_message(self, data: List[List[Union[str, int]]], max_widths: List[Optional[int]] = None, 
                          max_table_width: int = None, min_column_width: int = 10, 
                          fg_color: str = None, bg_color: str = None, table_outline_color=None,
                          header_color: Union[str, int] = None, cell_background: Union[str, int] = None, 
                          cell_color: Union[str, int] = None, update_type: str = "replace") -> None:
        """
        Creates and adds or updates a TableMessage in the panel according to the specified parameters.

        :param data: The data to be displayed in the table.
        :param max_widths: Optional list of maximum column widths.
        :param max_table_width: The maximum width of the table.
        :param min_column_width: The minimum width for each column.
        :param fg_color, bg_color, table_outline_color, header_color, cell_background, cell_color: 
            Formatting options for the table.
        :param update_type: Type of update ('replace', 'append').
        """
        fg_color = fg_color if fg_color is not None else self.fg_color
        bg_color = bg_color if bg_color is not None else self.bg_color
        cell_background = cell_background if cell_background is not None else self.bg_color
        table_outline_color = table_outline_color if table_outline_color is not None else self.fg_color
        header_color = header_color if header_color is not None else self.bg_color
        cell_color = cell_color if cell_color is not None else self.bg_color
        table_msg = TableMessage(self.term, data, max_widths, max_table_width, min_column_width,
                                 fg_color, bg_color, table_outline_color, header_color,
                                 cell_background, cell_color)

        # Format and update the table message
        formatted_table = table_msg.format_message()
        if update_type == 'replace':
            self.data = [table_msg]
            self.formatted_lines = formatted_table
        elif update_type == 'append':
            self.data.append(table_msg)
            self.formatted_lines.extend(formatted_table)


    def print_formatted_data(self, formatted_line:str, row:int):
        """
        Prints a formatted data line at the specified row within the panel.

        :param formatted_line: Formatted data line to print.
        :param row: Row number where the line should be printed.
        """
        with self.term.location(self.data_x, self.data_y + row):
            print(formatted_line, end="")
            
    def display_data(self) -> None:
        """
        Updates the displayed data based on the current top row (scrolling).
        """
        # Clear the panel content
        if self.panel_hidden:
            self.hide_panel
            return

        self.clear_panel(data_clear=True)

        # Determine the range of data rows to display based on scrolling
        start_idx = self.data_top
        end_idx = min(self.data_top + self.data_height, len(self.formatted_lines))

        # Display the data within the panel
        for i, line in enumerate(self.formatted_lines[start_idx:end_idx]):
            self.print_formatted_data(line, i)
        
        self.print_horizontal_borders()
        self.print_vertical_borders()

    def init_panel(self) -> None:
        """
        Sets the background color for the entire panel.

        :param bg_color: The background color to set for the panel.
        :type bg_color: str
        """
        # Redraw the panel with the new background color
        if self.panel_hidden != True:
            self.print_horizontal_borders()
            self.print_vertical_borders()
            self.display_data()


    def clear_panel(self, bg_color: Union[str, int] = None, data_clear: bool = False):
        """
        Clears the content of the panel used before updating size.

        Parameters:
        bg_color (Union[str, int]): The background color to use when clearing the panel.
        data_clear (bool): Flag to determine if the term_bg_color should be used.

        """
        # Determine the background color based on the provided parameters and defaults
        if data_clear and self.bg_color is not None:
            bg_color = self.bg_color
        elif not data_clear and self.term_bg_color is not None:
            bg_color = self.term_bg_color
        else:
            bg_color = self.bg_color

        width = self.panel_width
        formatted_row = ColorFormatter.apply_color(self.term, " " * width, bg_color, background=True)

        # Move to each line and clear it
        for row in range(self.panel_height):
            with self.term.location(self.panel_x, self.panel_y + row):
                print(formatted_row, end="")
    
    def clear_panel_data(self, bg_color: Union[str, int] = None):
        """
        Clears the content of the panel used before updating size.
        """
        bg_color = self.bg_color if bg_color is None else bg_color
        width = self.data_width
        formatted_row = ColorFormatter.apply_color(self.term, " " * width, bg_color, background=True)

        # Move to each line and clear it
        for row in range(self.panel_height):
            with self.term.location(self.panel_x, self.panel_y + row):
                print(formatted_row, end="")

        # Move the cursor away after clearing
        print(self.term.move(self.panel_y + self.panel_height, 0), end="")
    
    def hide_panel(self, bg_color:Union[str,int] = None):
        self.panel_hidden = True
        bg_color = self.term_bg_color if bg_color is None else bg_color
        self.clear_panel(bg_color)


    def scroll_up(self, highlight_selection=False, highlight_selection_bg = None):
        """Scrolls the data content up by one row."""
        if self.data_top > 0:
            self.data_top -= 1
            self.display_data()
            if highlight_selection:
                self.print_borders(highlight_selection=True, highlight_selection_bg=highlight_selection_bg)

    def scroll_down(self, highlight_selection=False, highlight_selection_bg = None):
        """Scrolls the data content down by one row."""
        if self.data_top + self.data_height < len(self.formatted_lines):
            self.data_top += 1
            self.display_data()
            if highlight_selection:
                self.print_borders(highlight_selection=True, highlight_selection_bg=highlight_selection_bg)
    
