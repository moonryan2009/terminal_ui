import logging

from src.panels.panel import DisplayPanel

class PanelManager:
    def __init__(self, term):
        self.term = term
        self.panels = {}
        
    def add_panel(self, *args, **kwargs):
        """Adds a DisplayPanel to the PanelManager with specified parameters.

        :param name: Name of the panel.
        :type name: str
        :param term: Terminal object from the blessed library.
        :type term: Terminal
        :param panel_x: X-coordinate for the panel's position.
        :param y: Y-coordinate for the panel's position.
        :param width: Width of the panel.
        :param height: Height of the panel.
        :param fg_color: Foreground color for the panel, defaults to None.
        :param bg_color: Background color for the panel, defaults to None.
        :type name: str
        :type term: Terminal
        :type panel_x: int
        :type y: int
        :type width: int
        :type height: int
        :type fg_color: str, optional
        :type bg_color: str, optional
        """
        # Create a DisplayPanel with the given arguments
        if 'term' not in kwargs:
            kwargs['term'] = self.term
        
         # Ensure 'name' is provided and is unique
        if 'name' not in kwargs:
            logging.warning("A unique 'name' must be provided for the panel.")
            raise ValueError("A unique 'name' must be provided for the panel.")
        
        panel_name = kwargs['name']
        if panel_name in self.panels:
            logging.warning(f"A panel with the name '{panel_name}' already exists.")
            raise ValueError(f"A panel with the name '{panel_name}' already exists.")

        new_panel = DisplayPanel(*args, **kwargs)

        # Add the panel to the UI's list of panels
        self.panels[panel_name] = new_panel

        logging.debug(f'Successfully added {panel_name}')
    
    def remove_panel(self,panel_name) -> None:
        """Removes a panel from the manager by its name.

        :param panel_name: The name of the panel to be removed.
        :type panel_name: str
        :raises ValueError: If the specified panel does not exist in the manager.
        """
        if panel_name not in self.panels:
            raise ValueError(f"Panel '{panel_name}' is not available cannot be removed.")
        del self.panels[panel_name]

        logging.debug(f'Successfully removed: {panel_name}')

    def get_panel(self, panel_name: str):
        """
        Retrieves a panel by its name and ensures it is expandable.

        :param panel_name: The name of the panel.
        :type panel_name: str
        :raises ValueError: If the panel does not exist or is not expandable.
        :return: The requested panel.
        :rtype: DisplayPanel
        """
        # Check if the panel exists
        panel = self.panels.get(panel_name)
        if not panel:
            raise ValueError(f"Panel '{panel_name}' does not exist.")

        return panel
    
    def toggle_panel(self, panel_name: str, excluded_panels: list = None):
        """
        Toggles the visibility of a specified panel and adjusts the size of all other panels.

        :param panel_name: The name of the panel to toggle.
        :param excluded_panels: List of panel names or patterns to exclude from resizing.
        """
        panel = self.get_panel(panel_name)
        
        if panel.expanded_size is None or panel.expanded_direction is None:
            raise ValueError(f"Panel '{panel_name}' is not expandable (missing expanded_size or expanded_direction).")
        
        is_panel_being_shown = panel.panel_hidden  # Check if the panel is currently hidden
        panel.panel_hidden = not panel.panel_hidden

        # Determine how much space the panel occupies when it's shown
        panel_space = panel.expanded_size if is_panel_being_shown else 0

        if panel.expanded_direction == 'right':
            # Adjust width
            new_width = self.term.width - panel_space
            self.adjust_horizontal_panel_sizes(new_width, excluded_panels=excluded_panels)
        elif panel.expanded_direction == 'bottom':
            # Adjust height
            new_height = self.term.height - panel_space
            self.adjust_vertical_panel_sizes(new_height, excluded_panels=excluded_panels)

    def adjust_vertical_panel_sizes(self, height:int, excluded_panels: list = None):
        """
        Adjusts the sizes of panels vertically based on the specified height.

        :param height: The height to adjust the panels to.
        """
        
        self.adjust_vertical_dynamic(height=height, excluded_panels=excluded_panels)
        #TODO
        #self.adjust_vertical_static(height)

    def adjust_vertical_dynamic(self, height: int = None, excluded_panels: list = None):
        """
        Dynamically adjusts the height of panels that are set to resize based on terminal height,
        excluding specified panels.

        :param height: The new height to set for dynamically resizable panels.
        :type height: int, optional
        :param excluded_panels: List of panel names or patterns to exclude from resizing.
        :type excluded_panels: list, optional
        """
        if excluded_panels is None:
            excluded_panels = []

        panels_adjusted = []
        for panel_name, panel in self.panels.items():
            if any(excluded_pattern in panel_name for excluded_pattern in excluded_panels):
                continue
            if not panel.panel_height_calculated:
                continue

            try:
                panel.resize_panel(term_height=height, redraw_panel=False)
                panels_adjusted.append(panel_name)
            except Exception as e:
                logging.error(f"Unable to vertically resize panel '{panel_name}': {e}")
        
        self.draw_panels()
        
        logging.debug(f'Vertically Resized: {panels_adjusted}')

    def adjust_horizontal_panel_sizes(self, width:int, excluded_panels: list = None):
        """
        Adjusts the sizes of panels horizontally based on the specified width.

        :param width: The width to adjust the panels to.
        """
        self.adjust_horizontal_dynamic(width=width, excluded_panels=excluded_panels)
        #TODO
        #self.adjust_vertical_static(height)

    def adjust_horizontal_dynamic(self, width: int = None, excluded_panels: list = None) -> None:
        """
        Adjusts the horizontal space of the panels based on the specified width, excluding specified panels.

        :param width: The width to resize the panels to.
        :type width: int
        :param excluded_panels: Panel names to exclude from resizing. Defaults to None, meaning no exclusions.
        :type excluded_panels: List[str], optional
        """
        if excluded_panels is None:
            excluded_panels = []

        resized_panels = []
        for panel_name, panel in self.panels.items():
            is_excluded = any(excluded_panel in panel_name for excluded_panel in excluded_panels)
            if is_excluded or not panel.panel_height_calculated:
                continue  # Skip excluded panels and non-dynamic panels

            try:
                panel.resize_panel(term_width=width, redraw_panel=False)
                resized_panels.append(panel_name)
            except Exception as e:
                logging.error(f"Error resizing panel '{panel_name}': {e}")

        logging.debug(f"Resized panels: {resized_panels}")
        
        self.draw_panels()


    def draw_panels(self):
        """
        Draws the entire UI by initializing and displaying each panel.

        This method loops through all panels and re-initializes them.
        """
        for panel_name, panel in self.panels.items():
            panel.init_panel()
    