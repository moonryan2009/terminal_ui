from src.utils.data_store import DataStore

class BaseApp:
    def __init__(self, input_callback=None, display_callback=None, display_table_callback=None, menu_callback=None):
        self.input_callback = input_callback
        self.display_callback = display_callback
        self.display_table_callback = display_table_callback
        self.menu_callback = menu_callback
        self.local_store = DataStore()
        self.menu_store = None

    def run(self):
        # Basic run logic - can be overridden in derived classes
        pass

    def add_menu_storage(self,menu_datastore):
        self.menu_store = menu_datastore

    # Other common methods and utilities
    def capture_user_input(self):
        if self.input_callback:
            return self.input_callback()
        else:
            raise NotImplementedError("Input callback not implemented.")

    def display_data(self, data, panel):
        if self.display_callback:
            self.display_callback(data, panel=panel)
        else:
            raise NotImplementedError("Display callback not implemented.")

    def display_data_table(self, table_data):
        if self.display_table_callback:
            self.display_table_callback(table_data)
        else:
            raise NotImplementedError("Display table callback not implemented.")

    def menu(self):
        pass
        #return
        #if self.menu_callback:
        #    self.menu_callback()
        #else:
        #    raise NotImplementedError("Menu callback not implemented.")