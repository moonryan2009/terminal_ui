import os
import platform
import logging
import time
from blessed import Terminal
from typing import List, Union, Optional

from src.panels.panel import DisplayPanel
from src.utils.color_formatter import ColorFormatter
from src.utils.hotkeys.hot_key import Hotkey
from src.utils.hotkeys.hot_key_manager import HotkeyManager
from src.panels.panel_manager import PanelManager
from src.menu.menu_manager import MenuManager
from src.menu.menu import Menu
from src.utils.logging_setup import setup_logging
from src.utils.log_handler import UILogHandler
from src.utils.data_store import DataStore

class TerminalSizeError(Exception):
    pass


class TerminalUI:

    """
    TerminalUI is a class for creating and managing a terminal-based user interface. 
    It handles the creation of various UI elements such as panels and supports hotkey bindings.

    Attributes:
        term (Terminal): An instance of the blessed Terminal class.
        fg_color (str): The default foreground color for UI elements.
        bg_color (str): The default background color for UI elements.
        min_size_x (int): The minimum width of the terminal for the UI.
        min_size_y (int): The minimum height of the terminal for the UI.
        highlight_selection_bg (Union[int, str]): The background color for highlighted selections.
        panels (dict): A dictionary to store the UI panels.
        hotkeys (dict): A dictionary to manage hotkey bindings.
    
    :param fg_color: Foreground color for the UI elements. Defaults to None.
    :type fg_color: str, optional
    :param bg_color: Background color for the UI elements. Defaults to None.
    :type bg_color: str, optional
    :param min_size_x: Minimum width of the terminal for the UI. Defaults to 80.
    :type min_size_x: int, optional
    :param min_size_y: Minimum height of the terminal for the UI. Defaults to 40.
    :type min_size_y: int, optional
    :param highlight_selection_bg: Background color for highlighted selections. Defaults to the foreground color.
    :type highlight_selection_bg: Union[int, str], optional

    The constructor initializes the TerminalUI instance, sets up panels, logging, and checks 
    for minimum terminal size requirements. It raises an exception if the initialization fails.
    """
    def __init__(self, fg_color: str = None, bg_color: str = None,
                    min_size_x: int = 80, min_size_y: int = 40,
                    highlight_selection_bg:Union[int,str]=None,
                    help_panel_height:int=5, debug_panel_width:int=40,
                    logging_level:int = logging.ERROR,
                    logging_file:str=None):
    
        setup_logging(filename=logging_file, level=logging_level)
        
        self.term = Terminal()
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.min_size_x = min_size_x
        self.min_size_y = min_size_y
        self.highlight_selection_bg = self.fg_color if highlight_selection_bg is None else highlight_selection_bg
        self.panels = {}
        self.hotkeys = {}
        self.original_widths = {}
        self.current_term_size = (self.term.width, self.term.height)
        self.last_term_size = self.current_term_size

        self.help_panel_height = help_panel_height
        self.debug_panel_width = debug_panel_width

        #for panel selection
        self.current_panel_index = 0
        self.previous_panel_index = -1
        
        self.global_data_store = DataStore()
        self.hotkey_manager = HotkeyManager()
        self.panel_manager = PanelManager(self.term)
        self.menu_manager = MenuManager()
        self.current_menu = None

        self.main_display_panel = None
        self.input_panel = None
        self.message_panel = None

        self.applications = {}


        try:
            self.add_panel(name='help_ui', term=self.term, 
                    offset_from_bottom=help_panel_height, offset_to_bottom=0,offset_to_right=0,
                    outline_top=True, panel_hidden=True,term_bg_color=self.bg_color,
                    expanded_direction='bottom', expanded_size=self.help_panel_height)
            self.add_panel(name='debug_ui', term=self.term, offset_to_right=0,
                    offset_from_right=self.debug_panel_width,
                    outline_left=True, panel_hidden=True,
                    expanded_direction='right',expanded_size=self.debug_panel_width)
            self.add_panel(name='min_size_ui', offset_to_right=0,
                    offset_to_bottom=0,panel_hidden=True)
        except Exception as e:
            #logging.error("Failed to initialize TerminalUI: %s", e)
            raise
            
        ui_log_handler = UILogHandler(self,panel='debug_ui')

        ui_log_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        logging.getLogger('').addHandler(ui_log_handler)
        self.check_min_size()
        #logging.basicConfig(level=logging.DEBUG,
        #            format='%(asctime)s - %(levelname)s - %(message)s')
        logging.debug('TerminalUI Initialized')

    def create_and_store_app(self, app_name, app_class, **kwargs):
        """
        Creates an application instance and stores it in the applications dictionary.

        :param app_name: Unique name for the application instance.
        :param app_class: The class of the application to be instantiated.
        :param kwargs: Additional keyword arguments to pass to the app's constructor.
        """
        app_instance = app_class(input_callback=self.user_input_callback,
                                 display_callback=self.display_data_callback,
                                 display_table_callback=self.display_data_table_callback,
                                 menu_callback=self.run,
                                 **kwargs)
        self.applications[app_name] = app_instance

    def launch_app(self, app_name):
        """
        Launches an application by its name.

        :param app_name: The name of the application to launch.
        """
        app_instance = self.applications.get(app_name)
        if app_instance:
            app_instance.run()
        else:
            logging.error(f"Application '{app_name}' not found.")

    def create_menu(self, menu_name, is_main_menu=False, add_common_items=False):
        """
        Creates a new menu and registers it with the MenuManager.

        :param menu_name: The name of the menu to create.
        :param is_main_menu: Flag to set this menu as the main menu. Defaults to False.
        :param add_common_items: Flag to add common items to this menu. Defaults to False.
        """
        new_menu = Menu()
        self.menu_manager.register_menu(menu_name, new_menu, is_main_menu, add_common_items)

    def add_menu_item(self, menu_name, item_name, item_description, action_or_navigation, use_menu_storage=True, **kwargs):
        """
        Adds an item to a specified menu. If the action is a string, it's assumed to be either an app name or another menu name.

        :param menu_name: The name of the menu to add the item to.
        :param item_name: The name of the menu item.
        :param item_description: The description of the menu item.
        :param action_or_navigation: The action associated with the menu item, the name of an app to launch, or a menu name for navigation.
        :param kwargs: Additional keyword arguments for the menu item.
        """
        if isinstance(action_or_navigation, str):
            if action_or_navigation in self.applications:
                # If it's an app name, create a lambda function to launch the app
                if use_menu_storage:
                    self.applications[action_or_navigation].add_menu_storage(self.menu_manager.get_menu(menu_name).menu_storage)
                action = lambda: self.launch_app(action_or_navigation)
            else:
                # If it's a menu name, create a lambda function for menu navigation
                action = lambda: self.menu_manager.set_current_menu(action_or_navigation)
        else:
            # If it's a callable, use it as the action
            action = action_or_navigation

        # Add the menu item to the specified menu
        menu = self.menu_manager.get_menu(menu_name)
        if menu:
            menu.add_item(item_name, item_description, action, **kwargs)
        else:
            logging.error(f"Menu '{menu_name}' not found.")
        
    def handle_terminal_resize(self):
        # Update current terminal size
        self.current_term_size = (self.term.width, self.term.height)

        if self.current_term_size != self.last_term_size:
            self.check_min_size()

            # Determine which dimension changed
            width_changed = self.current_term_size[0] != self.last_term_size[0]
            height_changed = self.current_term_size[1] != self.last_term_size[1]

            # Resize panels based on the dimension that changed
            if width_changed:
                self.panel_manager.adjust_horizontal_panel_sizes(self.term.width)
            if height_changed:
                self.panel_manager.adjust_vertical_panel_sizes(self.term.height)

            # Redraw all panels
            self.panel_manager.draw_panels()

            # Update last term size
            self.last_term_size = self.current_term_size
    
    def check_min_size(self):
        """
        Checks if the terminal size meets the minimum size requirements.

        This method clears the terminal and displays a warning message if the current
        terminal size is below the minimum required size. It continuously checks the
        terminal size and updates the display as necessary. The method exits if the
        terminal size exceeds the minimum requirements or if the user presses 'q' to quit.

        The minimum required size is defined by the min_size_x and min_size_y attributes.
        """
       
        if self.term.height >= self.min_size_y and self.term.width >= self.min_size_x:
            return
        
        TerminalUI.clear_terminal()

        panel = self.panel_manager.panels['min_size_ui']
        
        panel.add_data(f"Current size: {self.term.width} x {self.term.height}. Please increase size to {self.min_size_x} x {self.min_size_y} or press 'q' to quit.")
        panel.panel_hidden = False
        # Store initial dimensions for comparison
        last_width, last_height = self.term.width, self.term.height
        panel.display_data()

        while True:
            try:
                current_width, current_height = self.term.width, self.term.height

                # Check for dimension change in terminal
                if (current_width, current_height) != (last_width, last_height):
                    last_width, last_height = current_width, current_height
                    # Update and display panel only if size changes
                    TerminalUI.clear_terminal()
                    panel.resize_panel()
                    panel.add_data(f"Current size: {current_width} x {current_height}. Please increase size to {self.min_size_x} x {self.min_size_y} or press 'q' to quit.")
                    panel.display_data()

                # Exit if the size exceeds minimum requirements
                if current_width > self.min_size_x and current_height > self.min_size_y:
                    panel.add_data("Size okay. Press any key to continue.")
                    panel.display_data()
                    with self.term.cbreak():
                        self.term.inkey(timeout=10)  # Give the user a moment to read the message
                    TerminalUI.clear_terminal()
                    panel.panel_hidden = True
                    break

                # Allow user to quit
                with self.term.cbreak():
                    inp = self.term.inkey(timeout=1)
                    if inp.lower() == 'q':
                        TerminalUI.clear_terminal()
                        raise TerminalSizeError("User terminated the application due to insufficient terminal size.")
            except Exception as e:
                logging.error(f"Error in check_min_size: {e}")

    def clear_terminal(self) -> None:
        """
        Clears the content of the panel used before updating size.
        """
        width = self.term.width
        formatted_row = ColorFormatter.apply_color(self.term, " " * width, self.bg_color, background=True)

        # Build the entire block to clear
        clear_block = "\n".join([formatted_row] * self.term.height)

        # Move to the starting position and print the block once
        with self.term.location(0, 0):
            print(clear_block, end="")
    
    def run(self):
        self.check_min_size()

        # Store the initial terminal dimensions
        last_term_size = (self.term.width, self.term.height)
        
        # Display all panels initially
        for name, panel in self.panel_manager.panels.items():
            panel.display_data()

        # List of panel names for cycling
        previous_panel_names = []

        # reset panel indexes 
        self.current_panel_index = 0
        self.previous_panel_index = -1  # Initialize with an invalid index

        self.panel_manager.panels['help_ui'].add_data(self.hotkey_manager.hotkey_string)

        user_input = ('',False)
        
        with self.term.cbreak(), self.term.hidden_cursor():
            while True:

                self.check_min_size()
                self.handle_terminal_resize()

                if user_input[1]:
                    user_input = ('',False)        


                #Get the panel names for selection
                panel_names = [name for name in self.panel_manager.panels if not self.panel_manager.panels[name].panel_hidden]

                # Check if the list of panel names has changed
                if panel_names != previous_panel_names:
                    # Reset the current_panel_index if the list has changed
                    self.current_panel_index = 0
                    self.previous_panel_index = -1
                    previous_panel_names = panel_names.copy()

                key = self.term.inkey(.1)

                self.hotkey_manager.handle_hotkey(key.code)

                new_menu = self.menu_manager.get_current_menu()
                if new_menu != self.current_menu:
                    self.current_menu = new_menu
                    self.get_main_display_panel().add_table_message(self.menu_manager.get_current_menu().get_menu(), max_widths=[15,None,None], max_table_width=self.term.width -5)
                    self.get_main_display_panel().init_panel()

                # Update panel selection if a valid key is pressed
                if key.name in ["KEY_LEFT", "KEY_RIGHT"]:
                    self.handle_panel_navigation(key,panel_names)
                
                if key.name == "KEY_UP":
                    panel_name = panel_names[self.current_panel_index]
                    self.panel_manager.panels[panel_name].scroll_up(highlight_selection=True,highlight_selection_bg=self.highlight_selection_bg )
                elif key.name == 'KEY_DOWN':
                    panel_name = panel_names[self.current_panel_index]
                    self.panel_manager.panels[panel_name].scroll_down(highlight_selection=True,highlight_selection_bg=self.highlight_selection_bg)
                
                
                
                user_input = self.handle_user_input(key,prompt='Please enter a menu item or q to exit',current_input=user_input[0])

                if user_input[1]:
                    self.handle_menu_navigation(user_input[0])

                #if user_input[1]:
                #    message_panel = self.get_message_panel()
                #    message_panel.add_data(f'You entered: {user_input[0]}')
                #    message_panel.init_panel()

                
                # Exit loop if 'q' is pressed
                if user_input[0].lower() == 'q' and user_input[1]:
                    self.clear_terminal()
                    break
    
    def handle_menu_navigation(self, target) -> None:
        """
        Determines if the target is a menu or an application and performs the appropriate action.

        :param target: The name of the menu or application to navigate to or invoke.
        :type target: str
        """
        if target in self.menu_manager.menus:
            # Target is recognized as a menu; navigate to this menu.
            self.menu_manager.set_current_menu(target)
            logging.info(f"Navigated to menu '{target}'.")  # Documenting the navigation event
        elif target in self.applications:
            # Target is recognized as an application; launch this application.
            self.launch_app(target)
            logging.info(f"Application '{target}' invoked.")  # Documenting the invocation event
        else:
            # Target is neither a recognized menu nor an application; log this event as an error.
            logging.error(f"Target '{target}' not found as either a menu or an application.")
                

    def handle_panel_navigation(self, key: str, panel_names: list) -> int:
        """
        Handles navigation between panels using left and right arrow keys.

        This function updates the current panel index based on the key pressed (left or right) and
        skips any hidden panels.

        :param key: The key that was pressed.
        :type key: str
        :param panel_names: A list of the names of all panels.
        :type panel_names: list
        :param current_panel_index: The index of the currently selected panel.
        :type current_panel_index: int
        :return: The updated index of the currently selected panel.
        :rtype: int
        """
        self.previous_panel_index = self.current_panel_index

        if key.name == "KEY_LEFT":
            self.current_panel_index = (self.current_panel_index - 1) % len(panel_names)
        elif key.name == "KEY_RIGHT":
            self.current_panel_index = (self.current_panel_index + 1) % len(panel_names)

        # Update the panel selection if there's a change
        if self.current_panel_index != self.previous_panel_index:
            self.update_panel_selection(panel_names)

    def update_panel_selection(self, panel_names: list) -> None:
        """
        Updates the visual selection of panels based on the current and previous panel indices.

        This method de-highlights the previously selected panel and highlights the new current panel. 
        It is used to visually indicate which panel is currently active or focused in the UI.

        :param panel_names: A list of the names of all panels.
        :type panel_names: list
        :param current_panel_index: The index of the currently selected panel in the panel_names list.
        :type current_panel_index: int
        :param previous_panel_index: The index of the previously selected panel in the panel_names list.
                                    A value of -1 indicates that no panel was previously selected.
        :type previous_panel_index: int
        """
        # De-highlight the previous panel
        if self.previous_panel_index >= 0:
            previous_panel_name = panel_names[self.previous_panel_index]
            self.panel_manager.panels[previous_panel_name].print_borders(highlight_selection=False )

        # Highlight the new current panel
        current_panel_name = panel_names[self.current_panel_index]
        self.panel_manager.panels[current_panel_name].print_borders(highlight_selection=True, highlight_selection_bg=self.highlight_selection_bg)
        

    @staticmethod
    def clear_terminal() -> None:
        if platform.system() == "Windows":
            os.system('cls')
        else:
            # Covers macOS and Unix-like systems
            os.system('clear')
    


    def add_hotkey(self, key_str, function=None, label=None, toggle_panel=None, **kwargs):
        """
        Adds a new hotkey to the Terminal UI.

        :param key_str: The string representation of the hotkey (e.g., 'ctrl+n', 'f1').
        :param function: The function to be executed when the hotkey is pressed.
        :param label: An optional label for the hotkey.
        :param toggle_panel: The name of the panel to toggle, if applicable.
        :param kwargs: Additional keyword arguments for further configuration.
        """
        
        excluded_panels = kwargs.pop('excluded_panels', None)  # Extract excluded panels from kwargs
        
        if toggle_panel:
            # Create a lambda function for toggling a panel
            action = lambda: self.panel_manager.toggle_panel(toggle_panel, excluded_panels = excluded_panels)
        else:
            action = function

        # Pass the action and other parameters to the HotkeyManager
        self.hotkey_manager.add_hotkey(key_str, action, label, **kwargs)

    def remove_hotkey(self, *args,**kwargs):
        self.hotkey_manager.remove_hotkey(*args, **kwargs)


    def add_panel(self, *args, **kwargs):
        """
        Wrapper function to add a panel to the UI using PanelManager.

        This function takes the same arguments as the PanelManager's add_panel method.
        """
        self.panel_manager.add_panel(*args, **kwargs)
    
    def set_main_display_panel(self, panel_name):
        if panel_name in self.panel_manager.panels:
            self.main_display_panel = panel_name
        else:
            raise ValueError(f"Panel '{panel_name}' does not exist.")

    def set_input_panel(self, panel_name):
        if panel_name in self.panel_manager.panels:
            self.input_panel = panel_name
        else:
            raise ValueError(f"Panel '{panel_name}' does not exist.")

    def set_message_panel(self, panel_name):
        if panel_name in self.panel_manager.panels:
            self.message_panel = panel_name
        else:
            raise ValueError(f"Panel '{panel_name}' does not exist.")
    
    def get_main_display_panel(self):
        if self.main_display_panel and self.main_display_panel in self.panel_manager.panels:
            return self.panel_manager.panels[self.main_display_panel]
        else:
            raise ValueError("Main display panel is not set.")

    def get_input_panel(self):
        return self.panel_manager.panels.get(self.input_panel, self.get_main_display_panel())

    def get_message_panel(self):
        return self.panel_manager.panels.get(self.message_panel, self.get_main_display_panel())

    def handle_user_input(self, key, prompt: str, current_input: str = '', max_size: int = 100):
        input_panel = self.panel_manager.get_panel('prompt')
        max_input_length = max_size if max_size < 500 else 500
        user_input = current_input  # Initialize user_input with current_input

        input_panel.clear_panel(data_clear=True)
        input_panel.add_data(f"{prompt} {user_input}")
        input_panel.init_panel()

        if key.name == 'KEY_ENTER':
            return user_input, True
        elif key.name == 'KEY_BACKSPACE':
            user_input = user_input[:-1]
        elif key.is_sequence == False:
            if len(user_input) < max_input_length: 
                user_input += str(key)
            else:
                self.display_max_input_message(input_panel, prompt)

        if user_input != current_input:
            input_panel.clear_panel(data_clear=True)
            input_panel.add_data(f"{prompt} {user_input}")
            input_panel.init_panel()

        return user_input, False

    def capture_user_input(self, prompt):
        self.panel_manager.get_panel('prompt').clear_panel(data_clear=True)
        self.panel_manager.get_panel('prompt').add_data(f"{prompt} ")
        self.panel_manager.get_panel('prompt').init_panel()

        user_input = ""
        while True:
            key = self.term.inkey()
            user_input, done = self.handle_user_input(key, prompt, current_input=user_input)
            
            if done:
                break

        return user_input

    def display_max_input_message(self, input_panel, prompt):
        message = "Max input length reached. Press backspace to edit."
        if self.message_panel == self.input_panel:
            # If message panel is the same as input panel
            input_panel.add_data(message)
            input_panel.init_panel()
            time.sleep(5)  # Display message for 5 seconds
        else:
            # If message panel is different
            self.panel_manager.get_panel(self.message_panel).add_data(message)
            self.panel_manager.get_panel(self.message_panel).init_panel()

        # Redisplay the prompt
        input_panel.clear_panel(data_clear=True)
        input_panel.add_data(prompt)
        input_panel.init_panel()
    

    def display_data_callback(self, message: str, panel: str, **kwargs):
        """
        Displays a plain text message in a specified panel.

        :param message: The message to be displayed.
        :param panel: The name of the panel where the message will be displayed.
        :param kwargs: Additional keyword arguments for customization.
        """
        try:
            if panel == 'main':
                panel_obj = self.get_main_display_panel()
            else:
                panel_obj =  self.get_message_panel()
            panel_obj.add_data(message, **kwargs)
            panel_obj.init_panel()
            
        except Exception as e:
            logging.error(f"Failed to display data in {panel} panel: {e}")

    def display_data_table_callback(self, table: List[List[Union[str, int]]], panel: str, update_type: str = 'replace', **kwargs):
        """
        Displays data in a table format in a specified panel.

        :param table: The table data to be displayed (list of lists).
        :param panel: The name of the panel where the table will be displayed.
        :param update_type: The type of update to perform (e.g., 'replace', 'append').
        :param kwargs: Additional keyword arguments for customization.
        """
        try:
            if panel == 'main':
                self.get_main_display_panel().add_table_message(table, update_type=update_type, **kwargs)
            else: 
                self.get_message_panel().add_table_message(table, update_type=update_type, **kwargs)
        except Exception as e:
            logging.error(f"Failed to display table in {panel} panel: {e}")

    def user_input_callback(self, prompt: str):
        self.check_min_size()

        # Initialize variables for user input
        user_input = ''
        input_complete = False

        # Initialize panel names for navigation
        previous_panel_names = []

        # Reset panel indexes 
        self.current_panel_index = 0
        self.previous_panel_index = -1

        with self.term.cbreak(), self.term.hidden_cursor():
            while not input_complete:
                self.check_min_size()
                self.handle_terminal_resize()

                # Get the panel names for selection
                panel_names = [name for name in self.panel_manager.panels if not self.panel_manager.panels[name].panel_hidden]

                # Check if the list of panel names has changed
                if panel_names != previous_panel_names:
                    self.current_panel_index = 0
                    self.previous_panel_index = -1
                    previous_panel_names = panel_names.copy()

                key = self.term.inkey(.1)

                # Handle hotkeys and panel navigation
                self.hotkey_manager.handle_hotkey(key.code)
                if key.name in ["KEY_LEFT", "KEY_RIGHT"]:
                    self.handle_panel_navigation(key, panel_names)
                elif key.name == "KEY_UP":
                    panel_name = panel_names[self.current_panel_index]
                    self.panel_manager.panels[panel_name].scroll_up(highlight_selection=True,highlight_selection_bg=self.highlight_selection_bg)
                elif key.name == 'KEY_DOWN':
                    panel_name = panel_names[self.current_panel_index]
                    self.panel_manager.panels[panel_name].scroll_down(highlight_selection=True,highlight_selection_bg=self.highlight_selection_bg)

                # Process user input
                if key:
                    if key.name == 'KEY_ENTER':
                        input_complete = True
                    elif key.name == 'KEY_BACKSPACE':
                        user_input = user_input[:-1]
                    elif not key.is_sequence:
                        user_input += str(key)
                    # Update input panel with current user input
                    input_panel = self.panel_manager.get_panel('prompt')
                    input_panel.clear_panel(data_clear=True)
                    input_panel.add_data(f"{prompt} {user_input}")
                    input_panel.init_panel()

        self.clear_terminal()
        return user_input

    #     # UI-specific logic to display a message
    #     self.display_message_panel.update_data(message)

    # def request_input_callback(self, prompt, callback):
    #     # UI-specific logic to display a prompt and get input
    #     self.display_message_callback(prompt)
    #     user_input = self.get_user_input()  # Method to capture user input
    #     callback(user_input)
    
#     class MyApp:
#     def __init__(self, display_callback, input_callback):
#         self.display_callback = display_callback
#         self.input_callback = input_callback

#     def main(self):
#         # Use the display callback to show a message
#         self.display_callback("Welcome to MyApp!")

#         # Use the input callback to request additional input
#         def process_input(name):
#             self.display_callback(f"Name entered: {name}")
#             # Additional processing...
        
#         self.input_callback("Please enter your name: ", process_input)

# # In your UI setup
# ui_instance = MyUI()
# app = MyApp(ui_instance.display_message_callback, ui_instance.request_input_callback)