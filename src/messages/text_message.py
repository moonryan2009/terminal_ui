from typing import List
from blessed import Terminal

from src.utils.color_formatter import ColorFormatter
from src.messages.base_message import BaseMessage

class TextMessage(BaseMessage):
    def __init__(self, term: Terminal, text: str, max_width: int = None,
                 fg_color: str = None, bg_color: str = None, format: List[str] = None,
                 alignment: str = None):
        """
        Initializes a TextMessage object with specified properties for text formatting.

        :param term: A Blessed Terminal instance used for text formatting.
        :param text: The text to be formatted.
        :param max_width: The maximum width for text wrapping. If None, wrapping is disabled.
        :param fg_color: The foreground color to apply to the text. Can be a named color or
                         a number for 256-color mode.
        :param bg_color: The background color to apply to the text. Can be a named color or
                         a number for 256-color mode.
        :param format: A list of additional formatting styles to apply (e.g., 'bold', 'underline').
        :param alignment: The text alignment within its container. Can be 'left', 'right', or 'center'.
        :type term: Terminal
        :type text: str
        :type max_width: int, optional
        :type fg_color: str, optional
        :type bg_color: str, optional
        :type format: List[str], optional
        :type alignment: str, optional
        """
        super().__init__(term, max_width, fg_color, bg_color)
        self.alignment = alignment
        self.format = format
        self.text = text
        
    def format_message(self) -> List[str]:
        """Format the message text based on the specified styles and width.

        :return: List of formatted text lines.
        :rtype: List[str]
        """
        wrapped_text = self.term.wrap(self.text, self.max_width) if self.max_width else [self.text]
        formatted_lines = []

        for line in wrapped_text:
            formatted_line = line

            if self.alignment == 'center':
                formatted_line = self.center_text()
            
            if self.alignment == 'right':
                formatted_line = self.right_align_text()

            # Apply foreground color if set
            if self.fg_color is not None:
                formatted_line = ColorFormatter.apply_color(self.term, formatted_line, self.fg_color)

            # Apply background color if set
            if self.bg_color is not None:
                formatted_line = ColorFormatter.apply_color(self.term, formatted_line, self.bg_color, background=True)

            # Apply additional styles
            if self.format:
                for style in self.format:
                    if hasattr(self.term, style):
                        formatted_line = getattr(self.term, style)(formatted_line)
            
            formatted_lines.append(formatted_line)

        return formatted_lines
        
    def center_text(self) -> str:
        """Center-aligns the text based on the maximum width.

        :return: Center-aligned text.
        :rtype: str
        """
        text_width = len(self.text)
        if text_width >= self.max_width:
            return self.text  # Return the original text if it's longer than the width
        padding = (self.max_width - text_width) // 2
        return ' ' * padding + self.text + ' ' * padding
    
    def right_align_text(self) -> str:
        """Right-aligns the text based on the maximum width.

        :return: Right-aligned text.
        :rtype: str
        """
        text_width = len(self.text)
        if text_width >= self.max_width:
            return self.text  # Return the original text if it's longer than the width
        padding = self.max_width - text_width
        return ' ' * padding + self.text