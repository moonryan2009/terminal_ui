from typing import List, Union, Optional
from blessed import Terminal

from src.utils.color_formatter import ColorFormatter
from src.messages.base_message import BaseMessage

class TableMessage(BaseMessage):
    def __init__(self, term: Terminal, data: List[List[Union[str, int]]],
                 max_widths: List[Optional[int]] = None, max_table_width: int = None,
                 min_column_width: int = 10, fg_color: str = None,
                 bg_color: str = None, table_outline_color=None,
                 header_color:Union[str,int]=None, cell_background:Union[str,int]=None,
                 cell_color:Union[str,int]=None):
        """
        Initializes a DataTableMessage object with specified properties for table formatting.

        :param term: A Blessed Terminal instance used for text formatting.
        :param data: The data to be displayed in the table.
        :param max_widths: Optional list of maximum column widths.
        :param max_table_width: The maximum width of the table.
        :param min_column_width: The minimum width for each column.
        :param fg_color: The foreground color to apply to the table.
        :param bg_color: The background color to apply to the table.
        :param format: A list of additional formatting styles to apply.
        :param alignment: The text alignment within its container.
        """
        super().__init__(term, max_width=max_table_width, fg_color=fg_color, 
                         bg_color=bg_color)
        self.data = data
        self.max_widths = max_widths
        self.min_column_width = min_column_width
        self.table_outline_color = table_outline_color
        self.header_color = header_color
        self.cell_background = cell_background
        self.cell_color = cell_color
        self.num_columns =  len(self.data[0])
        self.column_widths = [0] * len(self.data[0])
    
    def format_message(self) -> List[str]:
    
        formatted_data = []

        if self.min_column_width * self.num_columns > self.max_width:
            raise ValueError("Min widths exceed max width")
        
        # Calculate natural width for each column
        for row in self.data:
            for i, cell in enumerate(row):
                self.column_widths[i] = max(self.column_widths[i], len(str(cell)), self.min_column_width)
        
                # Apply max widths if specified
        for i in range(self.num_columns):
            if self.max_widths and i < len(self.max_widths) and self.max_widths[i] is not None:
                self.column_widths[i] = min(self.column_widths[i], self.max_widths[i])

        total_width = sum(self.column_widths) + (2 * self.num_columns) + (self.num_columns - 1) + 2
        if total_width != self.max_width:
            self.adjust_column_widths()
        
        # Build and add the table header
        header_line = self.format_table_header_footer(self.column_widths)
        formatted_data.append(header_line)

        # Insert the header row and separator line
        header_row = self.format_table_line(self.data[0], True)
        formatted_data.append(header_row)
        separator_line = self.format_table_header_footer(self.column_widths)
        formatted_data.append(separator_line)

        # Build and add the rest of the table rows
        for row in self.data[1:]:  # Skip the first row as it's the header
            formatted_row = self.format_table_line(row, False)
            formatted_data.append(formatted_row)

        # Build and add the table footer
        footer_line = self.format_table_header_footer(self.column_widths)
        formatted_data.append(footer_line)
        return formatted_data

    def format_cell(self, cell, width, alignment='left', is_header=False):
        cell_content = str(cell)[:width] if len(str(cell)) > width else str(cell)
        cell_content = cell_content.center(width) if is_header else cell_content.ljust(width)
        # Apply color formatting
        color = self.header_color if is_header else self.cell_color
        return ColorFormatter.apply_color(self.term, cell_content, color, background=self.cell_background)

    
    def format_table_line(self, items, is_header=False):
        """
        Format a line in the table (either a row or a header) using class attributes.

        :param items: The items (cells) in the line.
        :param is_header: Flag indicating if the line is a header.
        :returns: The formatted line as a string.
        """

        formatted_items = [self.format_cell(cell, width, is_header=is_header) 
                           for cell, width in zip(items, self.column_widths)]

        # Apply color to the separator
        separator = '|'
        colored_separator = ColorFormatter.apply_color(self.term, separator, self.table_outline_color) if self.table_outline_color else separator
        colored_separator = ColorFormatter.apply_color(self.term, colored_separator, self.bg_color, background=True)
        # Join the formatted items with a separator and correct spacing
        line = f' {colored_separator} '.join(formatted_items)
        line = ColorFormatter.apply_color(self.term,line,self.bg_color,background=True)
        
        # Add the outside bars with correct spacing
        outside_bar = '|'
        colored_outside_bar = ColorFormatter.apply_color(self.term, outside_bar, self.table_outline_color)
        colored_outside_bar = ColorFormatter.apply_color(self.term, colored_outside_bar, self.bg_color, background=True)
        # Ensure spacing is consistent throughout the line
        return ColorFormatter.apply_color(self.term,f'{colored_outside_bar} {line} {colored_outside_bar}',self.bg_color,background=True)

    def format_table_header_footer(self, widths: List[int]) -> str:
        """
        Format a table header/footer line with color (if color schema is provided).

        This method creates a header/footer line for the table with color styling if a color schema is provided.

        :param widths: The list of column widths corresponding to each element in the header/footer line.
        :type widths: List[int]

        :returns: The formatted header/footer line with color styling (if color schema is available),
                  or a plain formatted line.
        :rtype: str
        """
        footer = '+' + '+'.join('-' * (w + 2) for w in widths) + '+'
        footer =  ColorFormatter.apply_color(self.term, footer, self.table_outline_color)
        return ColorFormatter.apply_color(self.term, footer, self.bg_color, background=True)


    def adjust_column_widths(self) -> None:
        """
        Adjust column widths to fit within the specified table width.

        This method adjusts the column widths to fit within the specified maximum table width while
        respecting maximum and minimum width constraints for individual columns.
        """
        difference = self.max_width - sum(self.column_widths) - (2 * len(self.column_widths)) - (len(self.column_widths) - 1) - 2
        flexible_columns = [i for i in range(len(self.column_widths)) if not self.max_widths or i >= len(self.max_widths) or self.max_widths[i] is None]

        while difference != 0 and flexible_columns:
            for i in flexible_columns:
                # Increase width if needed and no max width constraint or under max width
                if difference > 0 and (not self.max_widths or i >= len(self.max_widths) or self.max_widths[i] is None or self.column_widths[i] < self.max_widths[i]):
                    self.column_widths[i] += 1
                    difference -= 1
                # Decrease width if needed and above min width
                elif difference < 0 and self.column_widths[i] > self.min_column_width:
                    self.column_widths[i] -= 1
                    difference += 1
                if difference == 0:
                    break