from abc import ABC, abstractmethod
from typing import List
from blessed import Terminal

class BaseMessage:
    """This class is responsible for applying various text formatting options,
    including color, alignment, and other styles to a given text string.

    :param term: A Blessed Terminal instance used for text formatting.
    :param text: The text to be formatted.
    :param max_width: The maximum width for text wrapping. If None, wrapping is disabled.
    :param fg_color: The foreground color to apply to the text. Can be a named color or
                        a number for 256-color mode.
    :param bg_color: The background color to apply to the text. Can be a named color or
                        a number for 256-color mode.
    :param format: A list of additional formatting styles to apply (e.g., 'bold', 'underline').
    :param alignment: The text alignment within its container. Can be 'left', 'right', or 'center'.
    :type term: Terminal
    :type max_width: int, optional
    :type fg_color: str, optional
    :type bg_color: str, optional

    The class utilizes the Blessed Terminal capabilities to apply the specified
    formatting and color options. It also handles text alignment and wrapping
    based on the provided `max_width`.
    """
    def __init__(self, term:Terminal, max_width:int=None,
                 fg_color:str=None, bg_color:str=None):
        self.max_width = max_width
        self.fg_color = fg_color
        self.bg_color = bg_color
        self.term = term

    @property
    def max_width(self) -> int:
        """Gets the maximum width for text wrapping.

        :return: The maximum width value.
        :rtype: int
        """
        return self._max_width

    @max_width.setter
    def max_width(self, new_max_width: int) -> None:
        """Sets a new maximum width for text wrapping.

        :param new_max_width: The new maximum width value to set.
        :type new_max_width: int
        """
        self._max_width = new_max_width

    @abstractmethod
    def format_message(self) -> List[str]:
        """
        Formats the message based on the specified styles and width.

        This method must be implemented by all subclasses to handle their specific
        message formatting logic.

        :return: List of formatted text lines.
        :rtype: List[str]
        """
        pass