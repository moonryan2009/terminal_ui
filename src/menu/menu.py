import logging

from src.menu.menu_item import MenuItem
from src.utils.data_store import DataStore

class Menu:
    def __init__(self):
        self.items = []
        self.next_item_id = 0
        self.menu_storage = DataStore()
    

    def add_item(self, name, description, action, id=None, **kwargs):
        """
        Add a menu item to the menu. If an ID is not provided, automatically assigns one.

        :param name: Name of the menu item.
        :param description: Description of the menu item.
        :param action: Action associated with the menu item.
        :param id: Optional. The unique identifier for the menu item.
        :param kwargs: Additional keyword arguments.
        :raises ValueError: If the provided ID is already in use.
        """
        if id is None:
            id = self.next_item_id
            self.next_item_id += 1
        elif any(item.id == id for item in self.items):
            raise ValueError(f"Menu item ID {id} is already in use.")

        menu_item = MenuItem(name, description, action, id=id, **kwargs)
        self.items.append(menu_item)
        logging.debug(f'Added menu item: {menu_item}')

    
    def get_menu(self):
        """
        Returns the menu as a list of lists sorted by ID, with the first list being the headers.

        :return: List of lists representing the menu, sorted by item ID.
        """
        header = ["ID", "Name", "Description"]
        menu_list = [header]
        sorted_items = sorted(self.items, key=lambda item: item.id)
        for item in sorted_items:
            menu_list.append([item.id, item.name, item.description])
        return menu_list

    def exit_app(self):
        # Define exit logic
        pass

    # ... existing add_item and display methods ...
    
    

    def return_help_for_item(self, item_name):
        item = next((item for item in self.items if item.short_name == item_name), None)
        if item:
            return item.get_help_message()
        else:
            return ("No help available for this item.")