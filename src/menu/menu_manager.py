import logging
from src.menu.menu import Menu
from src.menu.menu_item import MenuItem

class MenuManager:

    def __init__(self, input_callback=None, display_data_callback=None, display_data_table_callback=None):

        self.input_callback = input_callback
        self.display_data_callback = display_data_callback
        self.display_data_table_callback = display_data_table_callback

        self.menus = {}
        self.menu_current = ''
        self.menu_main = ''
        self.menu_breadcrumbs = []

    def update_input_callback(self, new_callback):
        self.input_callback = new_callback

    def update_display_data_callback(self, new_callback):
        self.display_data_callback = new_callback

    def update_display_data_table_callback(self, new_callback):
        self.display_data_table_callback = new_callback


    def add_common_items(self):
        """Add common items like Exit, Return to Main, and Back."""
        self.add_item(MenuItem("Exit", "Exit the application", self.exit_app, id=0))
        if self.return_to_main:
            self.add_item(MenuItem(1, "Main", "Return to the main menu", self.goToMainMenu, id=1))
        if self.go_back:
            self.add_item(MenuItem(2, "Back", "Go back to the previous menu", self.previousMenu, id=2))
    


    def register_menu(self, name:str=None, menu:Menu=None, is_main_menu:bool=False, add_common:bool=False) -> None:
        """
        Registers a menu under a given name.

        :param name: The name under which the menu is registered.
        :param menu: The menu object to be registered.
        :param is_main_menu: If True, sets this menu as the main menu. Defaults to False.
        :type name: str
        :type menu: Menu
        :type is_main_menu: bool
        """
        if name in self.menus:
            logging.warning(f"A menu with the name '{name}' is already registered. Overwriting it.")
        
        if is_main_menu:
            self.menu_main = name
            logging.debug(f"Menu '{name}' set as the main menu.")
        
        self.menus[name] = menu
        self.menu_main = name
        logging.debug(f"Menu '{name}' registered successfully.")

        # Set the first registered menu as the current menu if none is set yet
        if not self.menu_current:
            self.set_current_menu(name)
        
        if add_common:
            self.menus[name].add_common_items()
    
    def set_current_menu(self, name:str) -> None:
        """
        Sets the current menu based on the provided name.

        :param name: The name of the menu to set as current. 
        :type name: str
        :return: None
        """
        if name in self.menus.keys():
            if name == self.menu_current:
                logging.debug(f'{name} is already selected')
                return
            if self.menu_current:
                self.menu_breadcrumbs.append(self.current_menu)
            self.menu_current = name
            logging.debug(f"Set {name} to current menu")
        else:
            logging.warning(f'{name} is not a valid menu')

    def get_menu(self, name: str):
        """
        Retrieves a menu by its name.

        :param name: The name of the menu to retrieve.
        :type name: str
        :return: The menu object if found, otherwise None.
        """
        return self.menus.get(name, None)

    def get_current_menu(self):
        """
        Retrieves the current menu.

        :return: The current menu object if set, otherwise None.
        """
        current_menu_name = self.menu_current
        return self.menus.get(current_menu_name, None)

    def previousMenu(self):
        """
        Navigates to the previous menu based on the navigation history.
        Updates the current menu to the previous one if available.
        """
        if self.menu_breadcrumbs:
            # Get the name of the last menu in the breadcrumbs
            previous_menu_name = self.menu_breadcrumbs.pop()
            previous_menu = self.menus.get(previous_menu_name, None)

            if previous_menu:
                # Set the previous menu as the current menu
                self.set_current_menu(previous_menu_name)
                logging.info(f"Navigated to previous menu: {previous_menu_name}")
            else:
                logging.warning(f"Previous menu '{previous_menu_name}' not found.")
        else:
            logging.info("No previous menu to navigate to.")
    
        return self.get_current_menu().get_menu()

    def goToMainMenu(self):
        """
        Navigates directly to the main menu.
        """
        if self.menu_main in self.menus:
            # Set the main menu as the current menu
            self.set_current_menu(self.menu_main)
            # Optionally, clear the breadcrumb history
            self.menu_breadcrumbs.clear()
            logging.info("Navigated to the main menu.")
        else:
            logging.warning("Main menu is not set or does not exist.")
        
        return self.get_current_menu().get_menu()
    
    def exit_app(self):
        return 'exit_app'
    

        