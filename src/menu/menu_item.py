class MenuItem:
    def __init__(self, name, description, action, id=None, **kwargs):
        """
        Initializes a new menu item.

        :param name: The name of the menu item.
        :param description: A brief description of the menu item.
        :param action: The function to be executed when this menu item is selected.
        :param id: An optional identifier for the menu item. If not provided, it can be set externally.
        :param kwargs: Additional keyword arguments that might be needed for future extensions.
        """
        self.id = id
        self.name = name
        self.description = description
        self.action = action
        self.kwargs = kwargs